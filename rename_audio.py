import os, sys
"""
This file renames all audio files such s1_u1.wav --> s01_u1.wav in "audio" folder.
This is needed in order to remain the sorting order in config  files like "wav.scp" in data folder.
because there is gonna be no order  between s1 , s10 , s11 .
"""
files="audio_files.txt"
with open(files) as f:
    init_files = f.readlines()
    init_files = [file.strip() for file in init_files]
out_files = ["audio/s0"+file.split("s")[1] for file in init_files]
#print(f"filenames are:\n{out_files[2]}")
#os.chdir("./audio")
[os.rename(init_name,out_name) for init_name,out_name in zip(init_files, out_files)]
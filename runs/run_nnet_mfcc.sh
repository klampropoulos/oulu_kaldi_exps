#!/bin/bash

. ./path.sh

. ./cmd.sh



gmmdir=exp/tri3b
data_dir=data/train 
ali_dir=exp/tri3b_ali
lang_dir=data/lang
dst_dir=exp/nnet 

rm -rf $dst_dir 


utils/subset_data_dir_tr_cv.sh $data_dir ${data_dir}_tr90 ${data_dir}_cv10

steps/nnet/train.sh --hid-layers 8 --hid-dim 256 --splice 11 \
--learn-rate 0.008  \
data/train_tr90 data/train_cv10  $lang_dir \
$ali_dir $ali_dir $dst_dir

steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" $gmmdir/graph data/test  $dst_dir/decode

for x in exp/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh ; done 
for x in exp/*/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done 


echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

#!/bin/bash

############## Universal training steps ##############################
prep_lang=true    # preparing language model features
prep_feats=true   # preparing acoustic features
mon_train=true     # monophone training
mon_ali=true      # monophone aligning
mon_dec=true       # monophone decoding
tri1_train=true      # triphone training
tri1_dec=true        #  triphone decoding 
tri1_align=true      # triphone aligning

##############  B recipe(OPTIONAL)         #####################################################
tri2b_train=true      # triphone training
tri2b_dec=true        #  triphone decoding 
tri2b_align=true      # triphone aligning
trimmi_train=false      # triphone training
trimmi_dec=false        #  triphone decoding 
trimpe_train=false       # triphone training
trimpe_dec=false         #  triphone decoding
tri3b_train=true        # triphone training
tri3b_dec=true          # triphone decoding
tri3b_align=true        #  triphone  align

##############  A recipe(OPTIONAL)          ########################################################


tri2a_train=false      # triphone training
tri2a_dec=false        #  triphone decoding 
tri2a_align=false       #  triphone  align
tri3a_train=false        # triphone training
tri3a_dec=false          # triphone decoding
tri3a_align=false        #  triphone  align
tri4a_train=false        # triphone training
tri4a_dec=false          # triphone decoding
tri4a_align=false        #  triphone  align


subset=false       # create a subset of train data
n_points=2000      # number of training points for subset
n_j=2
lm_order=1 # language model order (n-gram quantity) - 1 is enough for digits grammar

read -p "Choose recipe:(A/B):  " recipe
echo "Recipe is: $recipe"

: "${recipe:=B}"

if [ "$recipe" = A ]
then
    echo "Inside A"
    tri2a_train=true
    tri2a_dec=true
    tri2a_align=true
    tri3a_train=true
    tri3a_dec=true
    tri3a_align=true
    tri4a_train=true
    tri4a_dec=true
    tri4a_align=true
    
    tri2b_train=false  
    tri2b_dec=false
    tri2b_align=false
    tri3b_train=false 
    tri3b_dec=false
    tri3b_align=false

elif [ "$recipe" = B ]
then
    echo "Inside B"
 
else
    echo "Recipe error!!"
    exit 1;
fi

echo -e "TRI4A_TRAIN: $tri4a_train"


bst_sil=0.75
##### deltas(tri) training

numleaves=600
totgauss=2500
numleaves_3b=1000
totgauss_3b=6500

numleaves_2a=$numleaves
totgauss_2a=$totgauss
numleaves_3a=$numleaves
totgauss_3a=$totgauss
numleaves_4a=$numleaves
totgauss_4a=$totgauss



echo
echo "===== PREPARING ACOUSTIC DATA ====="
echo
# Needs to be prepared by hand (or using self written scripts):
#
# spk2gender [<speaker-id> <gender>]
# wav.scp [<uterranceID> <full_path_to_audio_file>]
# text [<uterranceID> <text_transcription>]
# utt2spk [<uterranceID> <speakerID>]
# corpus.txt [<text_transcription>]

. ./prep.sh || exit 1

. ./path.sh || exit 1
. ./cmd.sh || exit 1
. utils/parse_options.sh || exit 1

echo "train_mode : $train_mode"

#[[ $# -ge 1 ]] && { echo "Wrong arguments!"; exit 1; }
#exit 1;

# tool for data sorting if something goes wrong above
#moved from inside the loop
for x in train test
do 
    utils/fix_data_dir.sh data/$x
        
done





if [ "$prep_feats" = true ]; then
    echo
    echo "===== FEATURES EXTRACTION ====="
    echo
    
    mfccdir=mfcc
     
    
    
    # Making feats.scp files

    for x in train test 
    do
        steps/make_mfcc.sh --nj $n_j --cmd "$train_cmd" data/$x exp/make_mfcc/$x $mfccdir || exit 1;
    done
    # Making cmvn.scp files
    for x in train test 
    do
        steps/compute_cmvn_stats.sh data/$x exp/make_mfcc/$x $mfccdir || exit 1;
    done

    # script for checking if prepared data is all right
    for x in train test 
    do 
        utils/validate_data_dir.sh data/$x
        
    done


fi

#exit 1;

if [ "$prep_lang" = true ] ; then
    echo
    echo "===== PREPARING LANGUAGE DATA ====="
    echo
    # Needs to be prepared by hand (or using self written scripts):
    #
    # lexicon.txt [<word> <phone 1> <phone 2> ...]
    # nonsilence_phones.txt [<phone>]
    # silence_phones.txt [<phone>]
    # optional_silence.txt [<phone>]
    # Preparing language data
    utils/prepare_lang.sh data/local/dict "<UNK>" data/local/lang data/lang || exit 1;
    utils/validate_lang.pl data/lang/
    #exit 1;

    echo
    echo "===== LANGUAGE MODEL CREATION ====="
    echo "===== MAKING lm.arpa ====="
    echo

    loc=`which ngram-count`;
    if [ -z $loc ]; then
	    if uname -a | grep 64 >/dev/null; then
		    sdir=$KALDI_ROOT/tools/srilm/bin/i686-m64
	    else
		    sdir=$KALDI_ROOT/tools/srilm/bin/i686
	    fi
	    if [ -f $sdir/ngram-count ]; then
		    echo "Using SRILM language modelling tool from $sdir"
		    export PATH=$PATH:$sdir
	    else
		    echo "SRILM toolkit is probably not installed. Instructions: tools/install_srilm.sh"
		    exit 1
	    fi
    fi
    local=data/local
    mkdir $local/tmp
    ngram-count -order $lm_order -write-vocab $local/tmp/vocab-full.txt -wbdiscount -text $local/corpus.txt -lm $local/tmp/lm.arpa || exit 1

    echo
    echo "===== MAKING G.fst ====="
    echo

    lang=data/lang
    arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $local/tmp/lm.arpa > $lang/G.fst || exit 1;


fi



########## Training section #####################

####### MONOPHONE TRAINING ###########
#The listed argument options for this script indicate
#that we will take the first part of the dataset,
#followed by the location the data currently resides in,
#followed by the number of data points we will take (10,000), $n_points
#followed by the destination directory for the training data.
if [ "$subset" = true ]; then
    utils/subset_data_dir.sh --first data/train $n_points data/train_2k || exit 1;
    echo
    echo "== subset data created !!!!=="
    echo


fi

#Each of the training scripts takes a similar
#baseline argument structure with optional arguments
#preceding those. The one exception is the first monophone
#training pass. Since a model does not yet exist, there is
#no source directory specifically for the model.
#The argument --cmd “$train_cmd” designates which machine
#should handle the processing.Recall from above that we 
#specified this variable in the file cmd.sh.
if [ "$mon_train" = true ]; then
    echo
    echo "===== MONO_TRAINING ====="
    echo
    n_j=4
    steps/train_mono.sh --boost-silence $bst_sil --nj $n_j --cmd "$train_cmd" \
    data/train data/lang exp/mono || exit 1;

fi


if [ "$mon_dec" = true ]; then
    echo
    echo "===== MONO_DECODING ====="
    echo
    n_j=4
    utils/mkgraph.sh --mono data/lang exp/mono exp/mono/graph || exit 1;
    steps/decode.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" \
    exp/mono/graph data/test exp/mono/decode || exit 1; 
fi

# Just like the training scripts, the alignment scripts also adhere to the same argument structure.
if [ "$mon_ali" = true ]; then
    echo
    echo "===== MONO_ALIGN ====="
    echo
    n_j=4
    steps/align_si.sh --boost-silence $bst_sil  --nj $n_j --cmd "$train_cmd" \
    data/train data/lang exp/mono  exp/mono_ali || exit 1;

fi


if [ "$tri1_train" = true ]; then
    echo
    echo "===== TRI1 (first triphone pass) TRAINING ====="
    echo

    steps/train_deltas.sh --boost-silence $bst_sil --cmd "$train_cmd" $numleaves $totgauss data/train data/lang exp/mono_ali exp/tri1 || exit 1
fi


if [ "$tri1_dec" = true ]; then
    echo
    echo "===== TRI1 (first triphone pass) DECODING ====="
    echo
    utils/mkgraph.sh data/lang exp/tri1 exp/tri1/graph || exit 1
    steps/decode.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" exp/tri1/graph data/test exp/tri1/decode
fi



if [ "$tri1_align" = true ]; then
    echo
    echo "===== TRI1 (first triphone pass) ALIGNING ====="
    echo
    n_j=4
    # align tri1
    steps/align_si.sh --nj $n_j --cmd "$train_cmd" \
    --use-graphs true data/train data/lang exp/tri1 exp/tri1_ali
fi


#################################### tri2a == [deltas+deltas] ############################


if [ "$tri2a_train" = true ]; then
    echo
    echo "===== TRI2A   TRAINING ====="
    echo
 
    steps/train_deltas.sh --boost-silence $bst_sil --cmd "$train_cmd" $numleaves_2a $totgauss_2a data/train data/lang exp/tri1_ali exp/tri2a || exit 1
fi


if [ "$tri2a_dec" = true ]; then
    echo
    echo "===== TRI2A  DECODING ====="
    echo
    utils/mkgraph.sh data/lang exp/tri2a exp/tri2a/graph || exit 1
    steps/decode.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" exp/tri2a/graph data/test exp/tri2a/decode
fi



if [ "$tri2a_align" = true ]; then
    echo
    echo "===== TRI2A  ALIGNING ====="
    echo
    n_j=4
    # align tri1
    steps/align_si.sh --nj $n_j --cmd "$train_cmd" \
    --use-graphs true data/train data/lang exp/tri2a exp/tri2a_ali
fi



#################################### tri3a == [lda+mllt] ############################


if [ "$tri3a_train" = true ]; then
    echo
    echo "===== TRI3A   TRAINING ====="
    echo
    #numleaves_3a=2000
    #totgauss_3a=11000
    steps/train_lda_mllt.sh --cmd "$train_cmd" \
        $numleaves_3a $totgauss_3a data/train data/lang exp/tri2a_ali exp/tri3a || exit 1;

fi


if [ "$tri3a_dec" = true ]; then
    echo
    echo "===== TRI3A  DECODING ====="
    echo
    utils/mkgraph.sh data/lang exp/tri3a exp/tri3a/graph || exit 1
    steps/decode.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" exp/tri3a/graph data/test exp/tri3a/decode || exit 1;
fi



if [ "$tri3a_align" = true ]; then
    echo
    echo "===== TRI3A  ALIGNING LDA-MLLT triphones with FMLLR ====="
    echo
    n_j=4
    
    steps/align_fmllr.sh --nj $n_j --cmd "$train_cmd" \
        data/train data/lang exp/tri3a exp/tri3a_ali || exit 1;

fi


#################################### tri4a == [SAT triphones] ############################


if [ "$tri4a_train" = true ]; then
    echo
    echo "===== TRI4A   TRAINING ====="
    echo
    #numleaves_4a=1000
    #totgauss_4a=7000
    
    steps/train_sat.sh  --cmd "$train_cmd" \
        $numleaves_4a $totgauss_4a data/train data/lang exp/tri3a_ali exp/tri4a || exit 1;
fi


if [ "$tri4a_dec" = true ]; then
    echo
    echo "===== TRI4A  DECODING ====="
    echo
    utils/mkgraph.sh data/lang exp/tri4a exp/tri4a/graph || exit 1
    steps/decode.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" exp/tri4a/graph data/test exp/tri4a/decode
fi



if [ "$tri4a_align" = true ]; then
    echo
    echo "===== TRI4A  ALIGNING ====="
    echo
    n_j=4
    # align tri4
    steps/align_fmllr.sh --nj $n_j --cmd "$train_cmd" \
        data/train data/lang exp/tri4a exp/tri4a_ali || exit 1;
fi




###################################### tri2b == [LDA+MLLT] ##########################################

if [ "$tri2b_train" = true ]; then
    echo
    echo "===== TRI2B  TRAINING ====="
    echo
    n_splice=5   
    steps/train_lda_mllt.sh --cmd "$train_cmd" \
        --splice-opts "--left-context=$n_splice --right-context=$n_splice" \
        $numleaves $totgauss data/train data/lang exp/tri1_ali exp/tri2b
    
fi


if [ "$tri2b_dec" = true ]; then
    echo
    echo "===== TRI2B  DECODING ====="
    echo
    n_j=4
    utils/mkgraph.sh data/lang exp/tri2b exp/tri2b/graph
    steps/decode.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b/decode
fi

# you could run these scripts at this point, that use VTLN.
# local/run_vtln.sh
# local/run_vtln2.sh


if [ "$tri2b_align" = true ]; then
    echo
    echo "===== TRI2B  ALIGNING ====="
    echo
    # Align all data with LDA+MLLT system (tri2b)
    n_j=4
    steps/align_si.sh --nj $n_j --cmd "$train_cmd" --use-graphs true \
        data/train data/lang exp/tri2b exp/tri2b_ali
    
fi


###################################### Do MMI on top of LDA+MLLT. ##########################################(Not recommended)

if [ "$trimmi_train" = true ]; then
    echo
    echo "===== TRI2B-MMI TRAINING ====="
    echo
    n_j=4
    steps/make_denlats.sh --nj $n_j --cmd "$train_cmd" \
        data/train data/lang exp/tri2b exp/tri2b_denlats
    steps/train_mmi.sh data/train data/lang exp/tri2b_ali exp/tri2b_denlats exp/tri2b_mmi

    
fi


if [ "$trimmi_dec" = true ]; then
    echo
    echo "===== TRI2B-MMI  DECODING ====="
    echo
    n_j=4
    steps/decode.sh --config conf/decode.config --iter 4 --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b_mmi/decode_it4
    steps/decode.sh --config conf/decode.config --iter 3 --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b_mmi/decode_it3
fi




###################################### Do the same with boosting. ##########################################(Not recommended)




if [ "$tri2bmmi_train" = true ]; then
    echo
    echo "===== TRI2B-MMI-BOOST TRAINING ====="
    echo
    n_j=4
    steps/train_mmi.sh --boost 0.05 data/train data/lang \
        exp/tri2b_ali exp/tri2b_denlats exp/tri2b_mmi_b0.05 
fi


if [ "$tri2bmmi_dec" = true ]; then
    echo
    echo "===== TRI2B-MMI-BOOST  DECODING ====="
    echo
    n_j=4
    steps/decode.sh --config conf/decode.config --iter 4 --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b_mmi_b0.05/decode_it4
    steps/decode.sh --config conf/decode.config --iter 3 --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b_mmi_b0.05/decode_it3
fi


########################################### Do MPE. #######################################(Not recommended)

if [ "$trimpe_train" = true ]; then
    echo
    echo "===== TRI2B-MPE TRAINING ====="
    echo
    n_j=4
    steps/train_mmi.sh --boost 0.05 data/train data/lang \
        exp/tri2b_ali exp/tri2b_denlats exp/tri2b_mmi_b0.05 
fi



if [ "$trimpe_dec" = true ]; then
    echo
    echo "===== TRI2B-MPE  DECODING ====="
    echo
    n_j=4
    steps/decode.sh --config conf/decode.config --iter 4 --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b_mmi_b0.05/decode_it4
    steps/decode.sh --config conf/decode.config --iter 3 --nj $n_j --cmd "$decode_cmd" \
        exp/tri2b/graph data/test exp/tri2b_mmi_b0.05/decode_it3
fi

################################# TRI3B #############################3



if [ "$tri3b_train" = true ]; then
    echo
    echo "===== TRI3B  TRAINING ====="
    echo
    steps/train_sat.sh $numleaves_3b $totgauss_3b data/train data/lang exp/tri2b_ali exp/tri3b
fi


if [ "$tri3b_dec" = true ]; then
    echo
    echo "===== TRI3B  DECODING ====="
    echo
    n_j=4
    utils/mkgraph.sh data/lang exp/tri3b exp/tri3b/graph
    steps/decode_fmllr.sh --config conf/decode.config --nj $n_j --cmd "$decode_cmd" \
        exp/tri3b/graph data/test exp/tri3b/decode

fi



if [ "$tri3b_align" = true ]; then
    echo
    echo "===== TRI3B  ALIGN ====="
    echo
    n_j=4
    # Align all data with LDA+MLLT+SAT system (tri3b)
    steps/align_fmllr.sh --nj $n_j --cmd "$train_cmd" --use-graphs true \
        data/train data/lang exp/tri3b exp/tri3b_ali

fi


# # We have now added a script that will help you find portions of your data that
# # has bad transcripts, so you can filter it out.  Below we demonstrate how to
# # run this script.
if [ -d "exp/tri3b_ali" ]; then
    steps/cleanup/find_bad_utts.sh --nj $n_j --cmd "$train_cmd" data/train data/lang \
    exp/tri3b_ali exp/tri3b_cleanup
    # # The following command will show you some of the hardest-to-align utterances in the data.v
    cat exp/tri3b_cleanup/all_info.sorted.txt >> UTTS

fi



#Display results
for x in exp/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done

# Saving  results
date >> results/gmm_results
echo "Training_mode: $train_mode!" >> results/gmm_results
echo "Number of gaussians:$totgauss , Number of leaves:$numleaves,bst_sil:$bst_sil,n_splice=$n_splice" >> results/gmm_results
echo >> results/gmm_results
for x in exp/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/gmm_results; done
for x in exp/*/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done #>> results/gmm_results; done
./best_ser.sh >> results/gmm_results;


# Saving warnings
date >> WARNINGS
echo "Training_mode: $train_mode!" >> WARNINGS
echo "Number of gaussians:$totgauss , Number of leaves:$numleaves,bst_sil:$bst_sil,n_splice=$n_splice" >> WARNINGS
echo "Number of gaussians_3b:$totgauss_3b,Number of leaves:$numleaves_3b" >> results/gmm_results
echo >> WARNINGS
for dir in exp/*; do [ -d $dir/log ] && utils/summarize_warnings.pl $dir/log >> WARNINGS; done
echo >> WARNINGS

# Extended warnings to a file:(EXT_WARNINGS)
# ./get_warnings.sh

echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

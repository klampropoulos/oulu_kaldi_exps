#!/bin/bash

. ./path.sh

. ./cmd.sh



gmmdir=exp/mfcc/tri3b
data_dir=data/train 
ali_dir=exp/mfcc/tri3b_ali
lang_dir=data/lang
dst_dir=exp/nnet_fbank 


test=data-fbank/test
train=data-fbank/train

test_original=data/test
train_original=data/train



rm -rf $test
rm -rf $train
rm -rf $dst_dir

stage=0

. utils/parse_options.sh || exit 1;


#make fbank_feats
[ ! -e $test ] && if [ $stage -le 0 ]; then
  # Dev set
  utils/copy_data_dir.sh $test_original $test || exit 1; rm $test/{cmvn,feats}.scp
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd" \
     $test $test/log $test/data || exit 1;
  steps/compute_cmvn_stats.sh $test $test/log $test/data || exit 1;
  # Training set
  utils/copy_data_dir.sh $train_original $train || exit 1; rm $train/{cmvn,feats}.scp
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd --max-jobs-run 10" \
     $train $train/log $train/data || exit 1;
  steps/compute_cmvn_stats.sh $train $train/log $train/data || exit 1;
  # Split the training set
  utils/subset_data_dir_tr_cv.sh  $train ${train}_tr90 ${train}_cv10
fi

utils/subset_data_dir_tr_cv.sh $data_dir ${data_dir}_tr90 ${data_dir}_cv10

steps/nnet/train.sh --hid-layers 2 --hid-dim 256 --splice 11 \
--learn-rate 0.008 \
${train}_tr90 ${train}_cv10  $lang_dir \
$ali_dir $ali_dir $dst_dir

steps/nnet/decode.sh  --nj 4 --cmd "$decode_cmd" $gmmdir/graph $test  $dst_dir/decode

for x in exp/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/nnet_fbank_results ; done 
for x in exp/*/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/nnet_fbank_results; done 


echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

#!/bin/bash


# Run experiments
./run_gmm_av.sh mfcc  	# MFCC features only
./run_gmm_av.sh video 	# Video features only
./run_gmm_av.sh av    	# MFCC + video features (using early integration)

./run_gmm_av.sh fbank  # Filterbank features only
./run_gmm_av.sh av2    # Filterbank + video features (using early integration)

echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

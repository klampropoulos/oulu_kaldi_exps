#! /bin/bash

# Modified version of
# Copyright 2017  Ruhr-University Bochum (Author: Hendrik Meutzner)
# Apache 2.0.

# needed for GMM experiments
boost_silence=1.9
bst_sil=0.75

# hyperparameters

nj=2
lm_order=2 # language model order (n-gram quantity) - 1 is enough for digits grammar
stage=0

numleaves=600
totgauss=2500
numleaves_3b=1000
totgauss_3b=6500


# Define feature type
#feat="mfcc"		# MFCC features only
feat="fbank"	# Filterbank features only
#feat="video"	# Video features only
#feat="av"    # MFCC + video features (using early integration)
#feat="av2"   # Filterbank + video features (using early integration)



. ./prep.sh || exit 1;

. ./cmd.sh  || exit 1;

. ./path.sh || exit 1;

echo "train_mode : $train_mode"

. utils/parse_options.sh || exit 1


# Check for externally provided features
if [ $# -ge 1 ]; then
  feat=$1
fi

# tool for data sorting if something goes wrong above
#moved from inside the loop
for x in train test
do 
    utils/fix_data_dir.sh data/$x
        
done


# Setup feature directories
mfcc="feat/mfcc"
fbank="feat/fbank"
video="feat/video"
av="feat/av"
av2="feat/av2"

data="data"
lang="$data/lang"
dict="$data/local/dict"

# Setup feature file directory
featdir=feat/$feat
steps="steps"
utils="utils"
exp="exp"

# print setup
echo "Features: ${feat}"

#echo "Sleeping for 5s!"
sleep 1

prep_lang=true

if [ "$prep_lang" = true ] ; then
    echo
    echo "===== PREPARING LANGUAGE DATA ====="
    echo
    # Needs to be prepared by hand (or using self written scripts):
    #
    # lexicon.txt [<word> <phone 1> <phone 2> ...]
    # nonsilence_phones.txt [<phone>]
    # silence_phones.txt [<phone>]
    # optional_silence.txt [<phone>]
    # Preparing language data
    utils/prepare_lang.sh data/local/dict "<UNK>" data/local/lang data/lang || exit 1;
    utils/validate_lang.pl data/lang/
    #exit 1;

    echo
    echo "===== LANGUAGE MODEL CREATION ====="
    echo "===== MAKING lm.arpa ====="
    echo

    loc=`which ngram-count`;
    if [ -z $loc ]; then
	    if uname -a | grep 64 >/dev/null; then
		    sdir=$KALDI_ROOT/tools/srilm/bin/i686-m64
	    else
		    sdir=$KALDI_ROOT/tools/srilm/bin/i686
	    fi
	    if [ -f $sdir/ngram-count ]; then
		    echo "Using SRILM language modelling tool from $sdir"
		    export PATH=$PATH:$sdir
	    else
		    echo "SRILM toolkit is probably not installed. Instructions: tools/install_srilm.sh"
		    exit 1
	    fi
    fi
    local=data/local
    mkdir $local/tmp
    ngram-count -order $lm_order -write-vocab $local/tmp/vocab-full.txt -wbdiscount -text $local/corpus.txt -lm $local/tmp/lm.arpa || exit 1

    echo
    echo "===== MAKING G.fst ====="
    echo

    lang=data/lang
    arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $local/tmp/lm.arpa > $lang/G.fst || exit 1;


fi

rm -rf $featdir/*
mkdir -p $featdir

rm -rf feat/$feat/*
mkdir -p feat/$feat

rm -rf tmp/*
mkdir -p tmp


fe_list="train test" # sets used for feature extraction
for x in $fe_list; do

    # extract regular features
    if [ "$feat" = "mfcc" ] || [ "$feat" = "av" ]; then
      mkdir -p $mfcc
      data2=$data/mfcc
      rm -rf $data2/$x/*
      mkdir -p $data2/$x
      cp -R $data/$x/* $data2/$x
      echo Hello "$data2/$x"
      $steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $data2/$x $exp/make_mfcc/$x $mfcc || exit 1
      # Compute CMVN stats
      $steps/compute_cmvn_stats.sh $data2/$x $exp/make_mfcc/$x $mfcc || exit 1
    fi

    if [ "$feat" = "fbank" ] || [ "$feat" = "av2" ]; then
      mkdir -p $fbank
      data2=$data/fbank
      rm -rf $data2/$x/*
      mkdir -p $data2/$x
      cp -R $data/$x/* $data2/$x

      $steps/make_fbank.sh --nj $nj --cmd "$train_cmd" --fbank_config conf/fbank.conf $data2/$x $exp/make_fbank/$x $fbank || exit 1
      # Compute CMVN stats
      $steps/compute_cmvn_stats.sh $data2/$x $exp/make_fbank/$x $fbank || exit 1
    fi

    if [ "$feat" = "video" ] || [ "$feat" = "av" ] || [ "$feat" = "av2" ]; then
      mkdir -p $video
      data2=$data/video
      rm -rf $data2/$x/*
      mkdir -p $data2/$x
      cp -R $data/$x/* $data2/$x

      echo "Running make_video.sh"
      make_video.sh --nj $nj \
                          --cmd "$train_cmd" \
                          --audioRoot audio \
                          --videoRoot ${x}_video_1 \
                          $data2/$x \
                          $exp/make_video/$x \
                          $video || exit 1

      # Compute CMVN stats
      $steps/compute_cmvn_stats.sh $data2/$x $exp/make_video/$x $video || exit 1
    fi


    if [ "$feat" = "av" ] || [ "$feat" = "av2" ]; then

      data2=$data/$feat
      mkdir -p $data2

      if [ "$feat" = "av" ]; then
        # Append audio/video features
        $steps/append_feats.sh --nj $nj --cmd "$train_cmd" \
          $data/mfcc/$x $data/video/$x \
          $data2/$x $exp/make_${feat}/$x $featdir || exit 1
      elif [ "$feat" = "av2" ]; then
        # Append audio/video features
        $steps/append_feats.sh --nj $nj --cmd "$train_cmd" \
          $data/fbank/$x $data/video/$x \
          $data2/$x $exp/make_${feat}/$x $featdir || exit 1
      fi
      $steps/compute_cmvn_stats.sh $data2/$x $exp/make_${feat}/$x $featdir || exit 1
    fi

done # for x in $fe_list

data=$data/$feat
exp=$exp/$feat
mkdir -p $exp

#echo "stage 2 finished sleep for 5"
#sleep 1

# GMM training
if [ $stage -le 3 ]; then
  echo ""
  echo "Stage ${stage}: Starting GMM training"
  #rm -rf $exp/* $data/train/split*
  rm $data/train/split*

  $steps/train_mono.sh --nj $nj --cmd "$train_cmd" \
    --boost_silence $boost_silence \
    $data/train $lang $exp/mono0a || exit 1;

  #$utils/mkgraph.sh $lang $exp/mono0a $exp/mono0a/graph || exit 1;

  $steps/align_si.sh --nj $nj --cmd "$train_cmd" \
    --boost_silence $boost_silence \
    $data/train $lang $exp/mono0a $exp/mono0a_ali || exit 1;

  $steps/train_deltas.sh --cmd "$train_cmd" \
    --boost_silence $boost_silence \
    600 2500 $data/train $lang $exp/mono0a_ali $exp/tri1 || exit 1;

  #$utils/mkgraph.sh $lang $exp/tri1 $exp/tri1/graph || exit 1;

  $steps/align_si.sh --nj $nj --cmd "$train_cmd" \
    $data/train $lang $exp/tri1 $exp/tri1_ali || exit 1;
  # LDA-MLLT
  $steps/train_lda_mllt.sh --cmd "$train_cmd" \
    --splice-opts "--left-context=5 --right-context=5" \
    1000 4500 $data/train $lang $exp/tri1_ali $exp/tri2b || exit 1;

  $utils/mkgraph.sh $lang $exp/tri2b $exp/tri2b/graph || exit 1;

  $steps/align_si.sh --cmd "$train_cmd" --nj $nj \
    --use-graphs true $data/train $lang $exp/tri2b $exp/tri2b_ali || exit 1;
  # SAT TRAINING
  $steps/train_sat.sh \
    1500 6500 $data/train $lang $exp/tri2b_ali $exp/tri3b || exit 1;

  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    $data/train $lang $exp/tri3b $exp/tri3b_ali

  #$utils/mkgraph.sh $lang $exp/tri3b $exp/tri3b/graph || exit 1;
fi


echo "stage 3 finished sleep for 5"
sleep 5


# GMM decoding
if [ $stage -le 4 ]; then

  model_list="mono0a tri1 tri2b tri3b"
  #model_list="tri3b"

  echo ""
  echo "Stage ${stage}: Starting GMM decoding"
  for mdl in $model_list; do

    for x in test; do

      echo ""
      echo "Evaluating model $mdl for $x"

      if [ ! -f $exp/$mdl/graph/HCLG.fst ]; then
        $utils/mkgraph.sh $lang $exp/$mdl $exp/$mdl/graph || exit 1;
      fi

      $steps/decode_fmllr.sh --cmd "$decode_cmd" --nj $nj --num-threads 4  \
          $exp/$mdl/graph $data/$x $exp/$mdl/decode_$x  #&

      wait

      echo ""
      echo "$x set decoded!"
      #local/score.sh $data/$x $exp/$mdl/graph $exp/$mdl/decode_$x
    done

  done #mdl
fi #stage

echo "Lm order:" $lm_order > results/${feat}_results
for x in exp/*/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/${feat}_results; done
for x in exp/*/*/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/${feat}_results; done

echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

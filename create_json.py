import json

list_uter = ["one seven three five one six two six six seven",
            "four zero two nine one eight five nine zero four",
            "one nine zero seven eight eight zero three two eight",
            "four nine one two one one eight five five one",
            "eight six three five four zero two one one two",
            "two three nine zero zero one six seven six four",
            "five two seven one six one three six seven zero",
            "nine seven four four four three five five eight seven",
            "six three eight five three nine eight five six five",
            "seven three two four zero one nine nine five zero",
            "Excuse me",
            "Goodbye",
            "Hello",
            "How are you",
            "Nice to meet you",
            "See you",
            "I am sorry",
            "Thank you",
            "Have a good time",
            "You are welcome"]


list_uter = [utt.upper() for utt in list_uter]

counter = 1
json_file = "transcript.json"

with open(json_file, "w") as f:
    for x in list_uter:
        for i in range(3):
            line = {"text": x , "trans": "u"+str(counter)}
            json.dump(line, f)
            f.write("\n")
            counter += 1

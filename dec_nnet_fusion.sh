#! /bin/bash


### This script performs decision fusion 
### two neural nnnets
### modified version of kaldi's wsj's decode_score_fusion(# Copyright 2018        Tien-Hong Lo)

set -euo pipefail

cmd=run.pl 

stage=0
iter=final 
nj=4
output_name="output"
apply_exp=false  # Apply exp i.e. write likelihoods instead of log-likelihoods
compress=false    # Specifies whether the output should be compressed before
                  # dumping to disk
use_gpu=false
skip_diagnostics=true
extra_left_context=20
extra_right_context=0


minimize=false
skip_scoring=false
num_threads=1
min_lmwt=5
max_lmwt=15
acwt=1.0  # Just a default value, used for adaptation and beam-pruning..
post_decode_acwt=10.0 
frames_per_chunk=20
extra_left_context_initial=-1
extra_right_context_final=-1
beam=18.0 # beam for decoding.  Was 13.0 in the scripts.
lattice_beam=10.0 # this has most effect on size of the lattices.

word_determinize=false  # If set to true, then output lattice does not retain
                        # alternate paths a sequence of words (with alternate pronunciations).
                        # Setting to true is the default in steps/nnet3/decode.sh.
                        # However, setting this to false
                        # is useful for generation w of semi-supervised training
                        # supervision and frame-level confidences.
write_compact=true   # If set to false, then writes the lattice in non-compact format,
                     # retaining the acoustic scores on each arc. This is
                     # required to be false for LM rescoring undeterminized
                     # lattices (when --word-determinize is false)
#end configuration section.

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;



echo "$0 $@"

average=true
max_active=7000
min_active=200

data1=$1
data2=$2
graphdir=$3
model_dir1=$4
model_dir2=$5
dir=$6
scale1=$7
scale2=$8
num_sys=2
nj=4
model_dirs=($model_dir1 $model_dir2)
data_dirs=($data1 $data2)


echo "Data_folder 1 is : $data1"
echo "Data_folder 2 is : $data2"
echo "Graphdir is: $graphdir"
echo "Model_folder 1 is : $model_dir1"
echo "Model_folder 1 is : $model_dir2"
echo "Dir_output_folder 1 is : $dir"
echo "Num sys is : $num_sys"
echo "Scale 1 is : $scale1"
echo "Scale 2 is : $scale2"




for dirn in $data1 $data2 $graphdir ; do 
    [ ! -d $dirn ] && "$0: directory $dirn does't exist" && exit 1;
done

for f in $graphdir/words.txt $graphdir/phones/word_boundary.int ; do
  [ ! -f $f ] && echo "$0: file $f does not exist" && exit 1;
done


num_threads=4
# Possibly use multi-threaded decoder
thread_string=
[ $num_threads -gt 1 ] && thread_string="-parallel --num-threads=$num_threads"


rm -rf  $dir
mkdir -p $dir

mkdir -p $dir/temp

for i in `seq 0 $[num_sys-1]`; do
  srcdir=${model_dirs[$i]}
  
  model=$srcdir/final.nnet
  data=${data_dirs[$i]}
  
  sdata=$data/split$nj;
  cmvn_opts=`cat $srcdir/cmvn_opts` || exit 1;
  
  feats="ark,s,cs:apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp scp:$sdata/JOB/feats.scp ark:- |"

  if $apply_exp; then
    output_wspecifier="ark:| copy-matrix --apply-exp ark:- ark:-"
  else
    output_wspecifier="ark:| copy-feats --compress=$compress ark:- ark:-"
  fi

  gpu_opt="--use-gpu=no"
  gpu_queue_opt=

  if $use_gpu; then
    gpu_queue_opt="--gpu 1"
    gpu_opt="--use-gpu=yes"
  fi

  echo "$i $model";
  models[$i]="ark,s,cs:nnet-compute    \
     '$model' '$feats' '$output_wspecifier' |"
done

# remove tempdir
rm -rf $dir/temp

# split data to nj
[[ -d $sdata && $data/feats.scp -ot $sdata ]] || split_data.sh $data $nj || exit 1;
echo $nj > $dir/num_jobs


# Assume the nnet trained by 
# the same tree and frame subsampling factor.
mkdir -p $dir/log

#if [ -f $model ]; then
#  echo "$0: $model exists, copy model to $dir/../"
#  #cp $model $dir/../
#fi



lat_wspecifier="ark:|"
extra_opts=
if ! $write_compact; then
  extra_opts="--determinize-lattice=false"
  lat_wspecifier="ark:| lattice-determinize-phone-pruned --beam=$lattice_beam --acoustic-scale=$acwt --minimize=$minimize --word-determinize=$word_determinize --write-compact=false $model ark:- ark:- |"
fi

if [ "$post_decode_acwt" == 1.0 ]; then
  lat_wspecifier="$lat_wspecifier gzip -c >$dir/lat.JOB.gz"
else
  lat_wspecifier="$lat_wspecifier lattice-scale --acoustic-scale=$post_decode_acwt --write-compact=$write_compact ark:- ark:- | gzip -c >$dir/lat.JOB.gz"
fi

if [ $stage -le 0 ]; then  
  $cmd --num-threads $num_threads JOB=1:$nj $dir/log/decode.JOB.log \
     matrix-sum --scale1=$scale1 --scale2=$scale2 "${models[@]}" ark:- \| \
     latgen-faster-mapped$thread_string --lattice-beam=$lattice_beam --acoustic-scale=$acwt --allow-partial=true \
     --minimize=$minimize --max-active=$max_active --min-active=$min_active --beam=$beam \
     --word-symbol-table=$graphdir/words.txt ${extra_opts} "$model" \
     $graphdir/HCLG.fst ark:- "$lat_wspecifier"
fi

if [ $stage -le 1 ]; then
  if ! $skip_diagnostics ; then
    [ ! -z $iter ] && iter_opt="--iter $iter"
    steps/diagnostic/analyze_lats.sh --cmd "$cmd" $iter_opt $graphdir $dir
  fi
fi

if ! $skip_scoring ; then
  if [ $stage -le 2 ]; then
    [ ! -x local/score.sh ] && \
    echo "Not scoring because local/score.sh does not exist or not executable." && exit 1;
    echo "score best paths"
    [ "$iter" != "final" ] && iter_opt="--iter $iter"
	scoring_opts="--min_lmwt $min_lmwt"
    local/score.sh $scoring_opts --cmd "$cmd" $data $graphdir $dir
    echo "score confidence and timing with sclite"
  fi
fi



echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;
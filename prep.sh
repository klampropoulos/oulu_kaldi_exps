#!/bin/bash

# This shell scirpt creates the necesary tree-like
# hierarchy needed for a `kaldi-like` recipe
# by creating dyanmic links,sub-folders and .etc
#read -p "Enter mode of train:(train/phase2) " mode
#echo "Mode is: $mode"
# Train in (phrases) , decode phrases
: "${mode:=phase2}"

# Train in (phrases + digits) , decode only phrases
#: "${mode:=phase1_2}"

PROJECT_ROOT=`pwd`
export PROJECT_ROOT=`pwd`
echo -e "Project_root:\t $PROJECT_ROOT"
### Remove previously created data
rm -rf exp transcript.json exp/mono* mfcc/*.ark mfcc/*.scp mfcc data conf path.sh cmd.sh  steps src utils *.wav 
#echo "File structure after deleting:\n"
#ls -d */
#exit 1;

#Define conf-varibales
use_en=false
freq=48000
first_beam=10.0
beam=13.0
lattice_beam=6.0
dnn_lattice_beam=10.0


#echo "Here"


######### Renaming audio files #######################
if [ ! -f "audio_files.txt" ]
then
    if ls audio/s0* || grep
    then
        :
    else    
        ls audio/s?_* > audio_files.txt ||  exit 1;
        echo -e "\nRenaming audio files"
        python3 rename_audio.py || exit 1; 
        echo -e "\nFinished renaming audio files"
    fi
fi
# make dynamic  link to folders of the wsj `recipe`
# from wsj example(recipe) directory
if [ ! -d "steps" ]
then
    ln -s ../wsj/s5/steps . || exit 1;
fi

if [ ! -d "utils" ]
then
    ln -s ../wsj/s5/utils . || exit 1;
fi


# make dynamic link to the source code of `kaldi/src`  folder
if [ ! -d "src" ]
then
    ln -s ../../src . || exit 1;
fi


# copy path.sh bash script  of wsj `recipe` to local folder
if [ ! -f "path.sh" ]
then
    cp ../wsj/s5/path.sh . || exit 1;
    sed -i '1d' path.sh
    sed -i '1iexport KALDI_ROOT=`pwd`/../..' path.sh
    #sed -i '1iexport PROJECT_ROOT=`pwd`' path.sh
    echo -e "\nModified path.sh ....."
    sed -n "1p" < path.sh
    #sed -n "2p" < path.sh
    echo -e "\nExecuting: ./path.sh"
    ./path.sh
    #echo $PROJECT_ROOT
    echo -e "\nExported environmental variables!!!"
fi

# copy cmd.sh script  of wsj `recipe` to local folder
if [ ! -f "cmd.sh" ]
then
    #cp ../wsj/s5/cmd.sh . || exit 1;
    echo '# Insert the following text in cmd.sh
train_cmd="utils/run.pl"
decode_cmd="utils/run.pl"
cuda_cmd="utils/run.pl"' > cmd.sh || exit 1;
fi


# copy run.sh script  of wsj `recipe` to local folder
#if [ ! -f "run.sh" ]
#then
#    cp ../wsj/s5/run.sh run_base.sh || exit 1;
#fi

# create transcript.json if not already existed

if [ ! -f "transcript.json" ]
then
    echo "Creating transcript.json......."
    python3 create_json.py
    echo "Finished" || exit 1;
fi

#echo "There"

# Create dynamic ink for local folder
if [ ! -d "local" ]
then
    #mkdir local || exit 1;
    #cp ../voxforge/s5/local/score.sh ./local/score.sh || exit 1;
    echo "Copying mini_librispeech/local here!!!"
    cp  -r ../mini_librispeech/s5/local . || exit 1;
fi



if [[ "$mode" = train || "$mode" = phase1_2 ]]
then
    if [ ! -f "lexicon.dict" ]
    then
        echo "Please put into your project folder"
        echo "a file named `lexicon.dict` with the following format:"
        echo "NINE  N AY N"
        echo "............."
        echo "HAVE  HH AE V"
        echo "Exiting....."
        exit 1;

    fi
elif [ "$mode" = phase2 ]
then    
    if [ ! -f "lexicon2.dict" ]
    then
        echo "Please put into your project folder"
        echo "a file named `lexicon2.dict` with the following format:"
        echo "NINE  N AY N"
        echo "............."
        echo "HAVE  HH AE V"
        echo "Exiting....."
        exit 1;
    fi
else
    echo "Mode error!!"
    exit 1;
fi

#echo "There"

# Create necessary directories
mkdir exp 
mkdir conf
mkdir data
# go to `data` subfolder and create more folders
#~/kaldi/egs/$PROJECT_NAME/data/local
cd data
mkdir test
mkdir train
mkdir lang
mkdir local
#echo "Hello"
# go to `local` subfolder and create more folders
# ~/kaldi/egs/$PROJECT_NAME/data/local
cd local
mkdir dict
echo -e "\nMaking dict/pron data --> python prep_dict.py"
cd dict

if [[ "$mode" = train || "$mode" = phase1_2 ]]
then
    lx_file=lexicon.dict
    ln $PROJECT_ROOT/$lx_file . || exit 1;
    python3 $PROJECT_ROOT/prep_dict.py --mode train || exit 1; #$mode || exit 1;
    # Delete unnecessary files !!!!!
    rm $lx_file
elif [ "$mode" = phase2 ]
then
    lx_file=lexicon2.dict
    #lx_file=lex_stress_extended.dict
    ln $PROJECT_ROOT/$lx_file . || exit 1;
    python3 $PROJECT_ROOT/prep_dict.py --mode train_phase2 || exit 1;
    # Delete unnecessary files !!!!!
    rm $lx_file
else
    echo "Mode error!!!!"
    exit 1;
fi   

echo -e "\nFinished with dict-pron files!!!\n" 

## Create files for data/train
## The files in `data/train` contain information regarding the 
## specific audio files,transcripts and speakers.

# go one level up in `data/train`
echo `pwd`
cd ../../train
# Create text files needed for training
touch text
touch spk2gender
#touch segments  segements files may not to be needed
touch wav.scp 
touch utt2spk
touch spk2utt
#################### data/train/ ###########################

echo -e "\nMaking acousting data --> python prep_acs_data.py"
if [[ "$mode" = train || "$mode" = phase1_2 ]]
then
    python3 $PROJECT_ROOT/prep_acs_data.py --mode train || exit 1;
elif [ "$mode" = phase2 ]
then
    python3 $PROJECT_ROOT/prep_acs_data.py --mode train_phase2 || exit 1;
else
    echo "Mode error!!!!"
    exit 1;
fi    
#python3 $PROJECT_ROOT/prepare_acs_data.py --mode train || exit 1;
#sort utt2spk -o utt2spk || exit 1
echo -e "\nCreating spk2tt !!!!!!..... perl utils/utt2spk_to_spk2utt.pl"
$PROJECT_ROOT/utils/utt2spk_to_spk2utt.pl utt2spk >  spk2utt   || exit 1;
echo -e "\nFinished with train-acs files!!!!!"


################# data/test/ ###########################
cd ../test
pwd
# Create text files needed for training
touch text
touch spk2gender
#touch segments  segements files may not to be needed
touch wav.scp 
touch utt2spk
touch spk2utt
####################
echo -e "\nMaking acousting data --> python prep_acs_data.py"
if [ "$mode" = train  ]
then
    python3 $PROJECT_ROOT/prep_acs_data.py --mode test || exit 1;
elif [[ "$mode" = phase2 || "$mode" = phase1_2 ]]
then
    python3 $PROJECT_ROOT/prep_acs_data.py --mode test_phase2 || exit 1;
else
    echo "Mode error!!!!"
    exit 1;
fi    
echo -e "\nCreating spk2tt !!!!!!..... perl utils/utt2spk_to_spk2utt.pl"
$PROJECT_ROOT/utils/utt2spk_to_spk2utt.pl utt2spk >  spk2utt   || exit 1;


############################## CONF-FILES ################################

# Create mfcc.conf in {my_porject}/conf folder
echo -e "\nCreating config files!!!!"
cd ../../ #go to {myproject} folder
cd conf
cat <<EOF >mfcc.conf
--use-energy=$use_en
--sample-frequency=$freq
EOF


#Create  decode.config in {my_project}/conf folder

cat <<EOF >decode.config
first_beam=$first_beam
beam=$beam
lattice_beam=$lattice_beam
EOF


cat <<EOF >mfcc_hires.conf
# config for high-resolution MFCC features, intended for neural network training.
# Note: we keep all cepstra, so it has the same info as filterbank features,
# but MFCC is more easily compressible (because less correlated) which is why
# we prefer this method.
--use-energy=false   # use average of log energy, not energy.
--sample-frequency=$freq #  Ouluvs2 is sampled at 48kHz
--num-mel-bins=40     # similar to Google's setup.
--num-ceps=40     # there is no dimensionality reduction.
#--low-freq=40    # low cutoff frequency for mel bins
#--high-freq=-200 # high cutoff frequently, relative to Nyquist of 24000 (=23800)
EOF

cat <<EOF >fbank.conf
--sample-frequency=$freq
--num-mel-bins=40
EOF

cat <<EOF >decode_dnn.config
beam=18.0 
lattice_beam=$dnn_lattice_beam
EOF


cat <<EOF >online_cmvn.conf
# configuration file for apply-cmvn-online, used in the script ../local/run_online_decoding.sh
EOF


echo -e "\nFinished with config files!!!!!"
cd ../ #go to {myproject} folder

######################### .gitignore files #################################

cat <<EOF >exp/.gitignore
# Ignore everything in this folder
*
# except this file
!.gitignore 

EOF

cat <<EOF >data/.gitignore
# Ignore everything in this folder
*
# except this file
!.gitignore 

EOF

# New code added 25/7/2019
for x in train test
do 
    utils/fix_data_dir.sh data/$x
        
done


export train_mode=$mode


echo -e "\nprep.sh-->Finished!!!!!"

#exit 0;
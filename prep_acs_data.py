import os, json
import sys
import argparse

PROJECT_ROOT = os.environ['PROJECT_ROOT']


def form_utter(mode, spk_len, utt_len, spk_str):
	"""Forms the list of strings of utterances 
		that will be needed for the creation of files.

	Arguments:
		mode: str (train/test/train_phase2/test_phase2)
		
		spk_len: int

		utt_len: int

		split_spk_str: int (speaker of which train/test split happens)

	"""
	if mode == "train":

		utter1_list = ["s"+str(i)+"_u"+str(j) for i in range(10, spk_len) for j in range(1, utt_len)
						 if (i!=29) and not(i==3 and j==21)]
        			
	    # spk < 10
		utter2_list = ["s0"+str(i)+"_u"+str(j) for i in range(1, 10) for j in range(1, utt_len) 
						if (i!=29) and not(i==3 and j==21)]
	                
	    # Please read the README.txt file provided with the dataset 
	    # no. 29 speaker is missing and an utterance(no 21) from speaker no. 3
		utters = utter1_list + utter2_list
	   
	elif mode == "test":

  		utters = ["s"+str(i)+"_u"+str(j) for i in range(spk_str, spk_len) for j in range(1, utt_len) 
                		if (i!=29) and not(i==3 and j==21) ]
    

	elif mode == "train_phase2":

		utter1_list = ["s"+str(i)+"_u"+str(j) for i in range(10, spk_len) for j in range(31, utt_len) 
                		if (i!=29) and not(i==3 and j==21) ]
	    # spk < 10
		utter2_list = ["s0"+str(i)+"_u"+str(j) for i in range(1, 10) for j in range(31, utt_len)
	                	if (i!=29) and not(i==3 and j==21) ]
	          
		utters = utter1_list + utter2_list

	elif mode == "test_phase2":

		#only utter(31-60) short phrases-->PHASE_2
		utters = ["s"+str(i)+"_u"+str(j) for i in range(spk_str, spk_len) for j in range(31, utt_len) 
                		if (i!=29) and not(i==3 and j==21) ]

	else:
		print(f"Mode that passed in {mode} was incorrect.Exiting!!!")
		sys.exit(1)

	return utters


def make_wav_csp(utterances):
	"""
	Creates `wav.scp` file.
	"""
	filenames = [ utter + ".wav" for utter in utterances] 
	audio_dir = PROJECT_ROOT + "/audio"	
	#audio_dir = "audio" local checking
	filepaths = [audio_dir + "/" + filename for filename in filenames]          				
	with open("wav.scp", "w") as wav_f:
		[wav_f.write(utt +" "+ fp + "\n") for utt,fp in zip(utterances, filepaths)]


def make_text_f(utterances):
	"""
	Creates `text` file.
	"""
	json_file = PROJECT_ROOT + "/" + "transcript.json"
	#json_file = "." + "/" + "transcript.json"
	with open(json_file) as js_f:
		data = [json.loads(line) for line in js_f ]
	utter_text = { string["trans"]:string["text"] for string in data }
	with open("text", "w") as f:
		for utt in utterances:
			index = utt.split("_")[1]
			text = utter_text[index]
			f.write(utt+" "+ text + "\n")
	#print(utter_text)
	return utter_text


def make_utt2spk_f(utterances):
	"""
	Creates `utt2spk` file.
	"""
	with open("utt2spk", "w") as f_utt: 
		[f_utt.write(utt +" "+ utt.split("_")[0] + "\n") for utt in utterances]


def make_spk2gender_f(mode, spk_str):
	"""
	Creates `spk2gender` file.
	"""
	
	gender_list = ["m", "f", "m", "m", "m", "m", "m", "m", "m",
	               "f", "f", "m", "m", "m", "m", "m", "m", "m", 
	               "m", "m", "m", "m", "m", "m", "m", "f", "m",
	               "f", " ", "m", "m", "m", "f", "m", "f", "m", 
	               "m", "m", "f", "f", "m", "m", "f", "m", "m", 
	               "m", "m", "f", "m", "f", "m", "m", "f"
	              ]

	if mode == "train" or mode == "train_phase2":
	    with open("spk2gender", "w") as f:
	        for i  in range(spk_str-1):
	            if i < 9:
	                f.write("s0"+str(i+1)+" "+gender_list[i] +"\n")
	                continue
	            if (i+1) == 29:
	                continue
	            f.write("s"+str(i+1)+" "+ gender_list[i] +"\n")

	elif mode == "test" or mode == "test_phase2" :
	    with open("spk2gender", "w") as f:
	        for i  in range(spk_str-1, len(gender_list)):
	            if i < 9:
	                f.write("s0"+str(i+1)+" "+gender_list[i] +"\n")
	                continue
	            if (i+1) == 29:
	                continue
	            f.write("s"+str(i+1)+" "+ gender_list[i] +"\n")
	else:
		print(f"Mode that passed in {mode} was incorrect.Exiting!!!")
		sys.exit(1)


def make_corpus_f(utterances, utter_text):
	"""
	Creates `corpus.txt` file
	"""
	unique_utts = [utt for i, utt in enumerate(utterances) if i % 3 == 0 and utt.split("_")[0] == "s01"]
	with open("corpus.txt", "w") as f_utt:
		for utt in unique_utts:
			ind=utt.split("_")[1]
			text = utter_text[ind]
			f_utt.write(text+"\n")
	with open("corpus.txt", "r") as f_r:
		lines = f_r.readlines()
	return lines 


def make_dict_f(unique_utts):
	"""
	Creates `dict.txt` file
	"""
	words = {word for line in unique_utts for word in line.split()}
	with open("dict.txt" , "w") as f:
		[f.write(word + "\n") for word in words]



if __name__=="__main__":
	#Argument parsing (e.g --mode train)
	parser = argparse.ArgumentParser()
	parser.add_argument("--mode", help="mode of execution: train/test")
	args=parser.parse_args()
	if args.mode == None:
		print("No --mode was specified!!Exiting....")
		sys.exit(1)
	if args.mode not in ["train", "train_phase2", "test", "test_phase2"]:
		print(f"Mode entered {args.mode} is incorrect!Please choose one from:\t(train/train_phase2/test/test_phase2)")
		sys.exit(1)
    	
	PROJECT_ROOT = os.environ['PROJECT_ROOT']
	spk_len= 54
	utt_len= 61
	split_spk_str= 42 # from which speaker the train/test split occurs!!!

	utters = form_utter(args.mode, spk_len, utt_len, split_spk_str)
	#Creates `wav.scp` file
	make_wav_csp(utters)
	#Creates `text` file
	utter_text = make_text_f(utters)
	#Creates `utt2spk` file
	make_utt2spk_f(utters)
	#Creates `spk2gender` file
	make_spk2gender_f(args.mode, split_spk_str)

	# Go to data/local
	os.chdir("../local")
	if args.mode == "train" or args.mode == "train_phase2":
		#Creates `corpus.txt` file
		uniq_utts = make_corpus_f(utters, utter_text)
		#Creates `dict.txt` file
		make_dict_f(uniq_utts)

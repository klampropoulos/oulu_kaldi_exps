#! /bin/bash

folder=test_audio # Folder that contains the .wav files of the test data
curr_files=`ls $folder/ | cut -d"." -f1`
levels_list="20 15 10 5 0 -5" #dB Signal to noise ratio

for level in $levels_list; do 
    mkdir ${folder}_$level || exit 1;
    for file in $curr_files; do
        audio-degradation-toolbox -d jsons/degradation_${level}.json $folder/$file.wav ${folder}_$level/$file.wav || exit 1;
    done
done


echo Success 
echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

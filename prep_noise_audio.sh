#! /bin/bash


#audioRoot=$1
#noiseRoot=$2

#echo "AudioRoot = " $audioRoot
#echo "noiseRoot = " $noiseRoot
audioRoot="audio"
data=data/test

noiseroot_list="test_audio_0 test_audio_5 test_audio_10 test_audio_15 test_audio_20 test_audio_-5"

for x in $noiseroot_list; do
    rm -rf data/$x
    mkdir data/$x
    cp -R $data/* data/$x
    echo "$DATA =" $data/$x
    sed -i "s|${audioRoot}|${x}|g" data/$x/wav.scp #| echo > data/$x/wav.scp
done

echo Success 
echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;
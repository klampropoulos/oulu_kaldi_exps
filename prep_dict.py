from shutil import copyfile
import argparse, sys


def clean_lines(lex_file, max_num=10):
    """
    Find bad symbols,rename original file (e.g lexicon2.dict, lexicon.dict)
    TO	T UW
    TO(2)	T IH
    TO(3)	T AH
    """
    with open(lex_file, "r") as f:
        lines = f.readlines()
    symbols = ["("+str(i)+")" for i in range(max_num)]
    #print(symbols)
    new_lines = []
    for line in lines:
        for symbol in symbols:
            if symbol in line:
                print("ha")
                new_lines.append(line.replace(symbol, ""))
                break
        else:
            new_lines.append(line)
    check = all(elem in new_lines for elem in lines)
    if check:
       pass
    else:
        lines = new_lines 
        with open(lex_file, "w") as f:
            f.writelines(lines)
    return lines


def find_unq_phones(lines):
    """
    Finds unique phonemes from lines of file

    Args:
        lines: list of str
    """   
    lines = [line.strip().split("\t")[1] for line in lines]
    lines = [line.split(" ") for line in lines]
    set_phonemes = set([x for l in lines for x in l])
    return set_phonemes


def find_strs_clust(phone_set):
    """
        Finds stress cluster from a set of phonemes.
    """

    strs_0 = set([ph  for ph in phone_set if "0" in ph])
    strs_1 = set([ph   for ph in phone_set if "1" in ph])
    strs_2 = set([ph  for ph in phone_set if "2" in ph])  
    stress_less = phone_set - strs_0 - strs_1 - strs_2
    #print(stress_less)
    #print(strs_0)
    #print(strs_1)
    #print(strs_2)
    list_comb = []
    list_unit = []
    for ph in strs_0:
        unit = ph
        #print(unit)
        #bool = unit + "1" in strs_1
        #print(bool)
        if ph[:-1] + "1" in strs_1:
            strs_1.discard(ph[:-1]+"1")
            unit = unit + " " + ph[:-1]+"1"
        if ph[:-1] + "2" in strs_2:
            strs_2.discard(ph[:-1]+"2")
            unit = unit + " " + ph[:-1]+"2"
        if unit != ph:
            #strs_0.discard(ph)
            list_unit.append(ph)
            list_comb.append(unit)
    [strs_0.discard(ph) for ph in list_unit]
    list_comb1 = []
    list_unit = []
    for ph in strs_1:
        unit = ph
        if ph[:-1] + "2" in strs_2:
            strs_2.discard(ph[:-1]+"2")
            unit = unit + " " + ph[:-1]+"2"
        if unit != ph:
            #strs_1.discard(ph)
            list_unit.append(ph)
            list_comb1.append(unit)
    [strs_1.discard(ph) for ph in list_unit]
    #print("After:")
    #print(list_comb)
    #print(list_comb1)
    #print(stress_less)
    #print(strs_0)
    #print(strs_1)
    #print(strs_2)
    return list_comb, list_comb1, strs_0, strs_1, strs_2, stress_less

if __name__ == "__main__":
    #Argument parsing (e.g --mode train)
    #"""
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", help="mode of execution: train/test")
    args=parser.parse_args()
    if args.mode == None:
        print("No --mode was specified!!Exiting....")
        sys.exit(1)
    if args.mode not in ["train", "train_phase2"]:
        print(f"Mode entered {args.mode} is incorrect!Please choose one from:\t(train/train_phase2)")
        sys.exit(1)

    if args.mode == "train":
        lex_file = "lexicon.dict"
    else:
        lex_file = "lexicon2.dict"
        #lex_file = "lex_stress_extended.dict"    
    lines = clean_lines(lex_file)
    set_phonemes = find_unq_phones(lines)
    #print(f"Set of phonemes is:\n{set_phonemes}")
    filename="nonsilence_phones.txt"    
    if args.mode == "train":
    
        with open(filename,"w") as f:
            for phone in set_phonemes:
                f.write(str(phone)+"\n")
    else:
        """
        it1,it2,it3,it4,it5,it6 = find_strs_clust(set_phonemes)
        with open(filename,"w") as f:
            for i in it1:
                f.write(str(i)+"\n")
            for i in it2:
                f.write(str(i)+"\n")
            for i in it3:
                f.write(str(i)+"\n")
            for i in it4:
                f.write(str(i)+"\n")
            for i in it5:
                f.write(str(i)+"\n")
            for i in it6:
                f.write(str(i)+"\n")
        """
        with open(filename,"w") as f:
            for phone in set_phonemes:
                f.write(str(phone)+"\n")
    filename="optional_silence.txt"
    with open(filename,"w") as f:
        f.write("SIL"+"\n")
    filename="silence_phones.txt"
    with open(filename,"w") as f:
        f.write("SIL"+"\n")
        f.write("SPN"+"\n")
    filename = "lexicon.txt"
    copyfile(lex_file, filename)
    with open(filename,"a+") as f:
        f.write("!SIL "+"SIL\n")
        f.write("<UNK> "+"SPN\n")
    #"""
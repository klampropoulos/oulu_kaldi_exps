#! /bin/bash

. ./path.sh

. ./cmd.sh

# This script performs avasr with early integration using  a BLSTM architecure!
# ==============================================================================
#

stage=0

set -eu

. utils/parse_options.sh || exit 1;

gmmdir=exp/mfcc/tri3b
#data_dir=data/train 
ali_dir=exp/mfcc/tri3b_ali
lang_dir=data/lang
nj=4

echo "Argument checking..."

if [[ "$#" -ge 1 ]] ; then
  view=$1
else
  read -p "Please give me view (1-5) for experiment: " view
fi


feat="av_ei_$view"

echo "Feature entered: $feat"

#===================================================== Noise-data ================================================================
test=data-fbank/test
train=data-fbank/train

test_original=data/test
train_original=data/train



rm -rf $test
rm -rf $train

########## For noise data #########
noise_list="test_audio_0 test_audio_5 test_audio_10 test_audio_15 test_audio_20 test_audio_-5"
fe_list="train test"

for x in $noise_list; do
  rm -rf data-fbank/$x
done

# Dev set
dt_fb=data-fbank
dt=data
exp=exp
data=data

################################################# make fbank_feats for noise ############################################## 
for x in $noise_list; do
  utils/copy_data_dir.sh $dt/$x $dt_fb/$x || exit 1; #rm $dt_fb/$x/{cmvn,feats}.scp 
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd"  $dt_fb/$x $dt_fb/$x/log $dt_fb/$x/data || exit 1;
  steps/compute_cmvn_stats.sh $dt_fb/$x $dt_fb/$x/log $dt_fb/$x/data || exit 1;

#  steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
#    $gmm/graph $dt_fb/$x $dir/decode_${x} || exit 1
done

#=====================================================================================================

################################################# make fbank_feats-cmvn for train,test ##############################################
[ ! -e $test ] && if [ $stage -le 0 ]; then
  # Dev set
  utils/copy_data_dir.sh $test_original $test || exit 1; ##rm $test/{cmvn,feats}.scp
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd" \
     $test $test/log $test/data || exit 1;
  steps/compute_cmvn_stats.sh $test $test/log $test/data || exit 1;
  # Training set
  utils/copy_data_dir.sh $train_original $train || exit 1; #rm $train/{cmvn,feats}.scp
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd --max-jobs-run 10" \
     $train $train/log $train/data || exit 1;
  steps/compute_cmvn_stats.sh $train $train/log $train/data || exit 1;
  # Split the training set
 
fi

############################## make video features ####################################################
data2=$dt_fb/video_${view}

for x in $fe_list ; do
  mkdir -p ${data2}_${x}
  cp -R $data/$x/* ${data2}_${x}
  echo "Running make_video.sh"
  ./make_video.sh --nj $nj \
    --cmd "$train_cmd" \
    --audioRoot audio \
    --videoRoot ${x}_video_${view} \
              ${data2}_${x} \
              $exp/make_video_${view}/$x \
              ${data2}_${x}/data || exit 1

  #        steps/compute_cmvn_stats.sh $data2/$x $exp/make_video_${view}/$x/log $data2/$x/data || exit 1;
done

#exit 0;

test=${data2}_test
train=${data2}_train

echo "test is:" $test 
echo "train is:" $train 
#rm $test/cmvn.scp

#rm $train/cmvn.scp

#data_dir=data/video_1/train

steps/compute_cmvn_stats.sh $test $test/log $test/data || exit 1;

steps/compute_cmvn_stats.sh $train $train/log $train/data || exit 1;

#####################################################append video + audio #######################################################

data2=$dt_fb/$feat
data=data
test=${data2}/test
train=${data2}/train

#featdir=${data2}

echo "test is:" $test 
echo "train is:" $train

######## append for train /test ###########
for x in $fe_list; do

    rm -rf $data2/$x/*
    mkdir -p $data2/$x
    cp -R $data/$x/* $data2/$x
    steps/append_feats.sh --nj $nj --cmd "$train_cmd" \
            $dt_fb/$x $dt_fb/video_${view}_$x \
            $data2/$x $exp/make_${feat}/$x $data2/$x/data || exit 1

    steps/compute_cmvn_stats.sh $data2/$x $exp/make_${feat}/$x $data2/$x/data || exit 1
done


utils/subset_data_dir_tr_cv.sh  $train ${train}_tr90 ${train}_cv10


for x in $noise_list; do
  
  rm -rf $data2/$x/*
  mkdir -p $data2/$x
  cp -R $dt_fb/$x/* $data2/$x
  steps/append_feats.sh --nj $nj --cmd "$train_cmd" \
            $dt_fb/$x $dt_fb/video_${view}_test \
            $data2/$x $exp/make_${feat}/$x $data2/$x/data || exit 1

    steps/compute_cmvn_stats.sh $data2/$x $exp/make_${feat}/$x $data2/$x/data || exit 1
done



rm -rf $exp/blstm4i_${feat}

dev=${data2}/test
gmm=exp/mfcc/tri3b

if [ $stage -le 1 ]; then
    dir=exp/blstm4i_${feat}
    ali=${gmm}_ali

    splice=3
    acwt=7.0
    num_layers=4
    learn_rate=0.00008
    # Train
    $cuda_cmd $dir/log/train_nnet.log \
    steps/nnet/train.sh --network-type blstm --learn-rate $learn_rate \
      --cmvn-opts "--norm-means=true --norm-vars=true" \
      --delta-opts "--delta-order=2" --feat-type plain --splice $splice \
      --scheduler-opts "--momentum 0.9 --halving-factor 0.5" \
      --train-tool "nnet-train-multistream-perutt" \
      --train-tool-opts "--num-streams=10 --max-frames=15000" \
      --proto-opts "--cell-dim 320 --proj-dim 200 --num-layers $num_layers" \
    ${train}_tr90 ${train}_cv10 data/lang $ali $ali $dir || exit 1;

    # Decode (reuse HCLG graph)
    steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
    $gmm/graph $dev $dir/decode || exit 1;
fi



for x in $noise_list; do
  rm -rf $dir/decode_${x}
  steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
        $gmm/graph $data2/$x $dir/decode_${x} || exit 1

done

# For stdout
for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh ; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done

# For writing to file
echo "Splice : $splice" >> results/blstm4i_${feat}
echo "acwt : $acwt" >> results/blstm4i_${feat}
echo "num_layers: $num_layers " >> results/blstm4i_${feat}
echo "Learn_rate: $learn_rate " >> results/blstm4i_${feat}

for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/blstm4i_${feat}; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/blstm4i_${feat}; done

#todo: sequence training


echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;
#! /bin/bash


. ./path.sh

. ./cmd.sh


stage=0

. utils/parse_options.sh || exit 1;


gmmdir=exp/mfcc/tri3b
data_dir=data/train 
ali_dir=exp/mfcc/tri3b_ali
lang_dir=data/lang



test=data-fbank/test
train=data-fbank/train

test_original=data/test
train_original=data/train



rm -rf $test
rm -rf $train

########## For noise data #########
noise_list="test_audio_0 test_audio_5 test_audio_10 test_audio_15 test_audio_20 test_audio_-5"
# Dev set
dt_fb=data-fbank
dt=data


################################################# make fbank_feats for noise ############################################## 
for x in $noise_list; do
  utils/copy_data_dir.sh $dt/$x $dt_fb/$x || exit 1; #rm $dt_fb/$x/{cmvn,feats}.scp 
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd"  $dt_fb/$x $dt_fb/$x/log $dt_fb/$x/data || exit 1;
  steps/compute_cmvn_stats.sh $dt_fb/$x $dt_fb/$x/log $dt_fb/$x/data || exit 1;

#  steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
#    $gmm/graph $dt_fb/$x $dir/decode_${x} || exit 1
done





################################################# make fbank_feats ##############################################
[ ! -e $test ] && if [ $stage -le 0 ]; then
  # Dev set
  utils/copy_data_dir.sh $test_original $test || exit 1; rm $test/{cmvn,feats}.scp
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd" \
     $test $test/log $test/data || exit 1;
  steps/compute_cmvn_stats.sh $test $test/log $test/data || exit 1;
  # Training set
  utils/copy_data_dir.sh $train_original $train || exit 1; rm $train/{cmvn,feats}.scp
  steps/make_fbank.sh --nj 10 --cmd "$train_cmd --max-jobs-run 10" \
     $train $train/log $train/data || exit 1;
  steps/compute_cmvn_stats.sh $train $train/log $train/data || exit 1;
  # Split the training set
  utils/subset_data_dir_tr_cv.sh  $train ${train}_tr90 ${train}_cv10
fi

utils/subset_data_dir_tr_cv.sh $data_dir ${data_dir}_tr90 ${data_dir}_cv10




##################### BLSTM-PERRUT ################################

dev=data-fbank/test
train=data-fbank/train

dev_original=data/test
train_original=data/train

#gmm=exp/tri3b
# Modified august 1
rm -rf exp/blstm4i


gmm=exp/mfcc/tri3b



if [ $stage -le 1 ]; then
  # Train the DNN optimizing per-frame cross-entropy.
  dir=exp/blstm4i
  ali=${gmm}_ali

  splice=3
  acwt=7.0
  cell_dim=320
  proj_dim=200
  num_layers=4
  num_streams=20
  # Train
  $cuda_cmd $dir/log/train_nnet.log \
    steps/nnet/train.sh --network-type blstm --learn-rate 0.00008 \
      --cmvn-opts "--norm-means=true --norm-vars=true" \
      --delta-opts "--delta-order=2" --feat-type plain --splice $splice \
      --scheduler-opts "--momentum 0.9 --halving-factor 0.5" \
      --train-tool "nnet-train-multistream-perutt" \
      --train-tool-opts "--num-streams=$num_streams --max-frames=15000" \
      --proto-opts "--cell-dim $cell_dim --proj-dim $proj_dim --num-layers $num_layers" \
    ${train}_tr90 ${train}_cv10 data/lang $ali $ali $dir || exit 1;

  # Decode (reuse HCLG graph)
  steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
    $gmm/graph $dev $dir/decode || exit 1;
fi


############### For noise data ####################
for x in $noise_list; do

    steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
        $gmm/graph $dt_fb/$x $dir/decode_${x} || exit 1

done

# For stdout
for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh ; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done



# For writing to file
echo "" >> results/blstm4i
echo "Splice : $splice, acwt : $acwt, cell_dim: $cell_dim, proj_dim: $proj_dim, num_layers: $num_layers, num_streams: $num_streams"  >> results/blstm4i
#echo "acwt : $acwt" >> results/blstm4i

for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/blstm4i; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/blstm4i; done

#todo: sequence training

: '
# Sequence training using sMBR criterion, we do Stochastic-GD with per-utterance updates.

dir=exp/blstm4i_smbr
srcdir=exp/blstm4i
#acwt=0.1



### ALIGNMENT AND LATTICES ####


if [ $stage -le 3 ]; then
  # First we generate lattices and alignments:
  steps/nnet/align.sh --nj 4 --cmd "$train_cmd" \
    $train data/lang $srcdir ${srcdir}_ali
  steps/nnet/make_denlats.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
    $train data/lang $srcdir ${srcdir}_denlats
fi






#lrate=0.000001 # an 10x smaller than with Sigmoid,
if [ $stage -le 4 ]; then
  # Re-train the DNN by 6 iterations of sMBR
  steps/nnet/train_mpe.sh --cmd "$cuda_cmd" --num-iters 6 --acwt 7.0 --do-smbr true \
    $train data/lang $srcdir ${srcdir}_ali ${srcdir}_denlats $dir
  # Decode
  for ITER in 6 3 1; do
    steps/nnet/decode.sh --nj 4 --cmd "$decode_cmd" --config conf/decode_dnn.config \
      --nnet $dir/${ITER}.nnet --acwt 7.0 \
      $gmm/graph $dev $dir/decode_it${ITER}
  done
fi

'


echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;
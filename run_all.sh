#! /bin/bash

# Baseline GMM-HMM model
./run_gmm.sh || exit 1;


# ASR NNET
# BLSTM DNN-HMM FBANK model
#./run_blstm.sh || exit 1;

### AVASR early integration

#./run_blstm_ei_av.sh 1 || exit 1;

#./run_blstm_ei_av.sh 2 || exit 1;

#./run_blstm_ei_av.sh 3 || exit 1;

#./run_blstm_ei_av.sh 4 || exit 1;

#./run_blstm_ei_av.sh 5 || exit 1;

### VSR NNET
#./run_blstm_video.sh 1 || exit 1;

#./run_blstm_video.sh 2 || exit 1;

#./run_blstm_video.sh 3 || exit 1;

#./run_blstm_video.sh 4 || exit 1;

#./run_blstm_video.sh 5 || exit 1;


### ASR NNET3
./run_nnet3_audio.sh || exit 1;



### VSR NNET3
./run_nnet3_video.sh 1 || exit 1;

./run_nnet3_video.sh 2 || exit 1;

./run_nnet3_video.sh 3 || exit 1;

./run_nnet3_video.sh 4 || exit 1;

./run_nnet3_video.sh 5 || exit 1;


### FUSION NNET3
./run_nnet3_fusion.sh || exit 1;


# Collect scores
python3 collect_results.py || exit 1;

echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;

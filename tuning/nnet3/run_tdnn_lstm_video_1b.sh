#! /bin/bash

# Set -e here so that we catch if any executable fails immediately
set -euo pipefail

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if [[ "$#" -ge 1 ]] ; then
  view=$1
else
  read -p "Please give me view (1-5) for experiment: " view

fi


fe_list="train test";
dt_fb=data-fbank
data=data
data2=$dt_fb/video_${view}
exp=exp
nj=4

if [ ! -d $data2 ]; then
    echo "Making: $data2 folder"
    for x in $fe_list ; do
      mkdir -p ${data2}_${x}
      cp -R $data/$x/* ${data2}_${x}
      echo "Running make_video.sh"
      ./make_video.sh --nj $nj \
      --cmd "$train_cmd" \
      --audioRoot audio \
      --videoRoot ${x}_video_${view} \
              ${data2}_${x} \
              $exp/make_video_${view}/$x \
              ${data2}_${x}/data || exit 1

    done

    test=${data2}_test
    train=${data2}_train
    steps/compute_cmvn_stats.sh $test $test/log $test/data || exit 1;
    steps/compute_cmvn_stats.sh $train $train/log $train/data || exit 1;
fi



if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

# training options
# training chunk-options
chunk_width=40,30,20
chunk_left_context=40
chunk_right_context=0
xent_regularize=0.1

# training options
decode_nj=4
srand=0
remove_egs=true
train_stage=-10
get_egs_stage=-10
common_egs_dir=tmp_egs
label_delay=5
stage=0
dir=exp/nnet3/tdnn_lstm_video_${view}
gmm_dir=exp/mfcc/tri3b
#gmm_dir=exp/mfcc/tri2b
train_set=data-fbank/video_${view}_train
ali_dir=${gmm_dir}_ali
graph_dir=$gmm_dir/graph
lang=data/lang

label_delay=5

rm -rf $dir
#mkdir $common_egs_dir

for f in $gmm_dir/final.mdl $train_set/feats.scp  \
    $gmm_dir/graph/HCLG.fst $ali_dir/ali.1.gz; do
  [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done



if [ $stage -le 1 ]; then
    mkdir -p $dir
    echo "$0: creating neural net configs using the xconfig parser";

    feat_dim=$(feat-to-dim scp:${train_set}/feats.scp - || exit 1;)
    num_targets=$(tree-info $ali_dir/tree |grep num-pdfs|awk '{print $2}')
    learning_rate_factor=$(echo "print (0.5/$xent_regularize)" | python)
    tdnn_opts="l2-regularize=0.05"
    output_opts="l2-regularize=0.01"
    lstm_opts="l2-regularize=0.01 decay-time=20 delay=-3 dropout-proportion=0.0"
    echo "Number of targets: $num_targets"
    echo "Feature(fbank) dimensions: $feat_dim"
    mkdir -p $dir/configs
    cat <<EOF > $dir/configs/network.xconfig
    input dim=$feat_dim name=input

    # please note that it is important to have input layer with the name=input
    # as the layer immediately preceding the fixed-affine-layer to enable
    # the use of short notation for the descriptor
    fixed-affine-layer name=lda delay=$label_delay input=Append(-1,0,1) affine-transform-file=$dir/configs/lda.mat


    relu-batchnorm-layer name=tdnn1 dim=150 $tdnn_opts
    relu-batchnorm-layer name=tdnn2 dim=150 $tdnn_opts input=Append(-1,0,1) 
    relu-batchnorm-layer name=tdnn3 dim=150 $tdnn_opts input=Append(-3,0,3)
    attention-relu-batchnorm-layer name=att key-dim=450 value-dim=150 num-left-inputs=50 num-right-inputs=50
    fast-lstmp-layer name=lstm3 cell-dim=520 recurrent-projection-dim=130 non-recurrent-projection-dim=130 #$lstm_opts
    output-layer name=output input=lstm3 output-delay=$label_delay dim=$num_targets max-change=1.5
EOF
    steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/

fi

if [ $stage -le 2 ]; then

    steps/nnet3/train_rnn.py --stage=$train_stage \
    --cmd="$decode_cmd" \
    --feat.cmvn-opts="--norm-means=true --norm-vars=true" \
    --trainer.srand=$srand \
    --trainer.max-param-change=2.0 \
    --trainer.num-epochs=14 \
    --trainer.deriv-truncate-margin=10 \
    --trainer.samples-per-iter=1000 \
    --trainer.optimization.num-jobs-initial=1 \
    --trainer.optimization.num-jobs-final=1 \
    --trainer.optimization.initial-effective-lrate=0.001 \
    --trainer.optimization.final-effective-lrate=0.001 \
    --trainer.rnn.num-chunk-per-minibatch=16 \
    --trainer.optimization.momentum=0.9 \
    --egs.chunk-width=$chunk_width \
    --egs.chunk-left-context=$chunk_left_context \
    --egs.chunk-right-context=$chunk_right_context \
    --egs.chunk-left-context-initial=0 \
    --egs.chunk-right-context-final=0 \
    --cleanup.remove-egs=$remove_egs \
    --use-gpu=wait \
    --feat-dir=$train_set \
    --ali-dir=$ali_dir \
    --lang=$lang \
    --dir=$dir  || exit 1;



fi


echo "stage 15 completed!!!"
decode_list=video_${view}_test
feat_dim_dec=$(feat-to-dim scp:data-fbank/${decode_list}/feats.scp - || exit 1;)
echo "Feature dimension of video features: $feat_dim_dec"

if [ $stage -le 4 ]; then
  frames_per_chunk=$(echo $chunk_width | cut -d, -f1)
  rm $dir/.error 2>/dev/null || true
  for dset in $decode_list; do
      (
      steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
          --acwt 1.0 --post-decode-acwt 10.0 \
          --extra-left-context $chunk_left_context \
          --extra-right-context $chunk_right_context \
          --extra-left-context-initial 0 \
          --extra-right-context-final 0 \
          --frames-per-chunk $frames_per_chunk \
         $graph_dir data-fbank/${dset} $dir/decode_${dset} || exit 1;
    ) || touch $dir/.error &
  done
  wait
  if [ -f $dir/.error ]; then
    echo "$0: something went wrong in decoding"
    exit 1
  fi
fi

# stdout
for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh ; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done

# write to file
for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/nnet3_video_${view}; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/nnet3_video_${view}; done

echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;



Execution markdown
===================



### Execution order
1. exec **run_gmm_new.sh** --> invokes A) sources **cmd.sh** (training , decoding configuration(machine-related))  B) sources **path.sh** (environmental variables  binaries path)
C) prep.sh --> (**prep_dict.py**, **prepare_acs_data.py**, dynamic linking of **src**, **steps**, **utils** from **wsj** recipe, creates **conf** files needed for training)
2. exec **prep_noise_audio.sh**
3. exec **run_blstm.sh**



### Additional scripts

 * **prep_video.sh** --> invokes **make_video.sh** which  creates needed folders , feats.scp  file needed  to run models with video features
 * **noise_augment.sh** --> creates test_audio_(dB) folders from test_audio folder (takes time)

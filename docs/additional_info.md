
Useful resources
=====================

![](https://cdn-images-1.medium.com/max/1200/1*BndwsZhpRaea3Ul62M1G4Q.png)


## Kaldi related resources!!!
***
<br>

### [Kaldi's official website](https://kaldi-asr.org/)


<br>

### *Tutorials:*

***

 1. **[Josh Meyer's Tutorial](http://jrmeyer.github.io/asr/2016/01/26/Installing-Kaldi.html)**
 <br>

 1. **[Kaldi for Dummies tutorial](http://www.kaldi-asr.org/doc/kaldi_for_dummies.html)**
<br>

 1. **[Eleanor Chodroff Tutorial](https://www.eleanorchodroff.com/tutorial/kaldi/)**
<br>


 1. **[WFSTs Youtube Video Lectures](https://www.youtube.com/channel/UCsg4NhZjN7NZuJIjwea0njw/videos)**

<br>
<br>

### *Useful papers-books-sites:*

***

<br>

 1. **[awesome-kaldi](https://github.com/YoavRamon/awesome-kaldi)**
<br>

 1. **[awesome-speech-papers](https://github.com/zzw922cn/awesome-speech-recognition-speech-synthesis-papers)**
<br>

 1. **[Speech zone website](http://www.speech.zone)**
<br>

 1. **[HTK BOOK](http://www.dsic.upv.es/docs/posgrado/20/RES/materialesDocentes/alejandroViewgraphs/htkbook.pdf)**
 
<br>

### *Misc Links:*

***
 1. **[Project's Source code link](https://gitlab.com/klampropoulos/oulu_kaldi_exps/tree/master)**
<br>

 1. **[Ouluvs2 dataset's homepage](http://www.ee.oulu.fi/research/imag/OuluVS2/)**




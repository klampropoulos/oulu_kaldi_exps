#! /bin/bash


. ./path.sh

. ./cmd.sh


stage=0

set -eu

. utils/parse_options.sh || exit 1;


gmmdir=exp/mfcc/tri3b
#data_dir=data/train 
ali_dir=exp/mfcc/tri3b_ali
lang_dir=data/lang
nj=4

#test=data-fbank/test
#train=data-fbank/train

#test_original=data/test
#train_original=data/train


echo "Argument checking..."

if [[ "$#" -ge 1 ]] ; then
  view=$1
else
  read -p "Please give me view (1-5) for experiment: " view

fi


#view=1
nj=4
fe_list="train test";
dt_fb=data-fbank
data=data
data2=$dt_fb/video_${view}
exp=exp


for x in $fe_list ; do
  mkdir -p ${data2}_${x}
  cp -R $data/$x/* ${data2}_${x}
  echo "Running make_video.sh"
  ./make_video.sh --nj $nj \
    --cmd "$train_cmd" \
    --audioRoot audio \
    --videoRoot ${x}_video_${view} \
              ${data2}_${x} \
              $exp/make_video_${view}/$x \
              ${data2}_${x}/data || exit 1

  #        steps/compute_cmvn_stats.sh $data2/$x $exp/make_video_${view}/$x/log $data2/$x/data || exit 1;
done

#exit 0;

test=${data2}_test
train=${data2}_train

echo "test is:" $test 
echo "train is:" $train 
#rm $test/cmvn.scp

#rm $train/cmvn.scp

#data_dir=data/video_1/train

steps/compute_cmvn_stats.sh $test $test/log $test/data || exit 1;

steps/compute_cmvn_stats.sh $train $train/log $train/data || exit 1;

utils/subset_data_dir_tr_cv.sh $train ${train}_tr90 ${train}_cv10



##################### BLSTM-PERRUT ################################

#dev=data/video_2/test
#train=data/video_2/train

#dev_original=data/test
#train_original=data/train

#gmm=exp/tri3b
# Modified august 1
rm -rf exp/blstm4i_video_${view}


gmm=exp/mfcc/tri3b



if [ $stage -le 1 ]; then
  # Train the DNN optimizing per-frame cross-entropy.
  dir=exp/blstm4i_video_${view}
  ali=${gmm}_ali

  splice=5
  acwt=7.0
  learn_rate=0.00008
  num_layers=2
  $cuda_cmd $dir/log/train_nnet.log \
    steps/nnet/train.sh --network-type blstm --learn-rate $learn_rate \
      --cmvn-opts "--norm-means=true --norm-vars=true" \
      --delta-opts "--delta-order=0" --feat-type plain --splice $splice \
      --scheduler-opts "--momentum 0.9 --halving-factor 0.5" \
      --train-tool "nnet-train-multistream-perutt" \
      --train-tool-opts "--num-streams=55 --max-frames=1500" \
      --proto-opts "--cell-dim 250 --proj-dim 120 --num-layers $num_layers" \
    ${train}_tr90 ${train}_cv10 data/lang $ali $ali $dir || exit 1;

  
  # Decode (reuse HCLG graph)
  steps/nnet/decode.sh --nj $nj --cmd "$decode_cmd" --config conf/decode_dnn.config --acwt $acwt \
    $gmm/graph $test $dir/decode || exit 1;
fi

# For stdout
for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh ; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done

# For writing to file
echo "Splice : $splice" >> results/blstm4i_video_${view}
echo "acwt : $acwt" >> results/blstm4i_video_${view}
echo "num_layers: $num_layers " >> results/blstm4i_video_${view}
echo "Learn_rate: $learn_rate " >> results/blstm4i_video_${view}
for x in $dir/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/blstm4i_video_${view}; done
for x in $dir/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/blstm4i_video_${view}; done

#todo: sequence training


echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;
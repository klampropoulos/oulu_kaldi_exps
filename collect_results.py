import os,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams


RESULTS_DIR="./results"
plt.style.use("fivethirtyeight")
CLEAN_SNR = 40 # APPROX NOT MEASURED


def visualize_and_report(diction, mode):
    """
    Args:

        diction: python dictionary

        mode: str

    """

    if mode is "audio":
        keys , values = [ int(key.split("_")[1])  if key != list(diction.keys())[0] else CLEAN_SNR for key in list(diction.keys()) ], list(diction.values())
        values = [float(value) for value in values]
        key_arr=np.array(keys)
        var_arr=np.array(values)
        for snr, ser in zip(key_arr, var_arr):
            if snr == CLEAN_SNR:
                snr="CLEAN"
            print(f"SNR = {snr} dB , SER = {ser}%")
        rcParams['figure.figsize'] = 16 , 9
        plt.yticks(range(0, 100, 2))
        plt.xticks(range(-5, 50, 5))
        plt.title("Audio results (SNR vs SER")
        plt.xlabel("$SNR$")
        plt.ylabel("$SER$")
        plt.scatter(key_arr, var_arr, marker="o")
        #plt.show()
        #plt.savefig(f"{mode}_results.png")
        plt.close()

    elif mode is "video":
        keys , values = [ int(key.split("_")[1])  for key in list(diction.keys()) ], list(diction.values())
        values = [float(value) for value in values]
        key_arr=np.array(keys)
        var_arr=np.array(values)
        for view, ser in zip(key_arr, var_arr):
            print(f"View = {view} , SER = {ser}%")
        plt.xlabel("$View$")
        plt.ylabel("$SER$")
        plt.title("Video results (View vs SER)")
        rcParams['figure.figsize'] = 16 , 9
        plt.yticks(range(80, 100, 1))
        plt.xticks(range(1, 6))
        plt.scatter(key_arr, var_arr, marker="o")
        #plt.savefig(f"{mode}_results.png")
        #plt.show()
        plt.close()
    else:
        print("Wrong mode!Exiting....")
        sys.exit(1)
        
    

def visualize_and_report_by_view(list_view):
    """
    Args:
        list_view : python list
    
    """

    snr_set = [-5, 0, 5, 10, 15, 20, CLEAN_SNR]
    i=0
    for diction in list_view:
        snrs , values = [ int(key.split("_")[1])  for key in list(diction.keys()) ], list(diction.values())
        alphas = [float(key.split("_")[2])  for key in list(diction.keys())]
        values = [float(value) for value in values]
        #print(snrs[1],alphas[1],values[1])
        print(f"\nView = {i+1}")
        for snr_val in snr_set:
            index_snr = [i for i, snr in enumerate(snrs) if snr==snr_val ]
            alphas_snr = [alphas[index] for index in index_snr]
            values_snr = [values[index] for index in index_snr]
            print(values_snr)
            min_val_index = values_snr.index(min(values_snr))
            min_alpha = alphas_snr[min_val_index]
            
            if snr_val == CLEAN_SNR:
                snr_val="CLEAN"

            print(f"For SNR = {snr_val} dB, best combination is : SER = {min(values_snr)}%, alpha = {min_alpha} , (1.0-alpha) = {1-min_alpha} ")

        i+=1
    

def parser():
    #To write all results(stdout) to a file instead of printing, uncomment the line below
    sys.stdout = open("results/sum_results", "w")

    # Parsing files from ./results folder to collect results
    # Naming conventions
    # ASR results: filename: nnet3
    # VSR results:  filenames: nnet3_video_{i} for i in range(1,5) [for each view]
    # AVASR RESULTS: filenames: nnet3_comb_{i} for i in range(1,5)  -||-
    filename = RESULTS_DIR + "/" + "nnet3"
    filenames = os.listdir(RESULTS_DIR)
    video_filenames = [RESULTS_DIR + "/" + file  for file in filenames if "nnet3_video" in file]
    comb_filenames = [RESULTS_DIR + "/" + file  for file in filenames if "comb" in file]

    # AUDIO
    print("\n============ASR results===============")

    with open(filename) as f:
        lines = f.readlines()
        # clean + 6 SNRs values = 7 last lines
        last_7 = [ line.strip() for line in lines[-7:]]
        ser_res_audio = { "SNR_"+ line.split()[1].split("/")[3].split("_")[3] if line != last_7[0] else "SNR_" + "clean" : line.split()[2] for line in last_7}

    visualize_and_report(ser_res_audio, "audio")



    # VIDEO
    print("\n============VSR results===============")

    ser_res_video = {}    
    for video_file in video_filenames:
        with open(video_file) as v_f: 
            lines = v_f.readlines()
            line = lines[-1]
            view = video_file.split("_")[-1]
            ser_res_video.update( { "view_" + view : line.split()[2] } )

    visualize_and_report(ser_res_video, "video")


    # FUSION
    print("\n=============Combination-fusion results===============")

    dict_view_list = [dict() for i in range(5)]
    i=0
    for comb_file in comb_filenames:
        #print(comb_file)
        with open(comb_file) as cmb_f:
            lines = cmb_f.readlines()
            for line in lines:
                #print(line)
                if  len(line.split()[1].split("/")[2].split("_")) >=7:

                    snr = line.split()[1].split("/")[2].split("_")[6]
                    ser =  line.split()[2]
                    alpha = line.split()[1].split("/")[2].split("_")[7]
                else: # CLEAN
                    snr = str(CLEAN_SNR)
                    ser =  line.split()[2]
                    alpha = line.split()[1].split("/")[2].split("_")[5]

                #print(i)    
                #print(type(snr),ser,alpha)
                dict_view_list[i].update( { view + "_" + snr + "_" + alpha : ser} )
            #print(i)
        i+=1
        

    visualize_and_report_by_view(dict_view_list)    


if __name__ == "__main__":
    parser()

#! /bin/bash


### Conducting experiments

test_list="test test_audio_0 test_audio_5 test_audio_10 test_audio_15 test_audio_20 test_audio_-5"
#: '
a=1
for i in $(seq 0.05 0.05 0.95); do
#for i in $(seq 0.15 0.05 0.95); do
#for i in $(seq 0.75 0.05 0.95); do
    j=`echo "1.0-$i" | bc`
    for test in $test_list; do
        for view in {1..5} ; do 
            ./dec_nnet3_fusion.sh data-fbank/$test data-fbank/video_${view}_test exp/mfcc/tri3b/graph \
            exp/nnet3/tdnn_lstm  exp/nnet3/tdnn_lstm_video_${view} \
            exp/nnet3/tdnn_lstm_comb_${view}_${test}_${i} $i $j || exit 1;

        done
    done
done

#'

###### Collecting results

for i in $(seq 0.05 0.05 0.95); do
    for test in $test_list; do
        for view in {1..5} ; do
            dir=exp/nnet3/tdnn_lstm_comb_${view}_${test}_${i} || exit 1;
            # stdout        
            #for x in $dir; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh ; done || exit 1;
            #for x in $dir; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh ; done  || exit 1;
            
            # write to file
            #for x in $dir; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/nnet3_comb_${view}; done || exit 1;
            for x in $dir; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/nnet3_comb_${view}; done  || exit 1;

        done
    done
done




echo "Finishing...!"
echo Success
echo "===== $0 script is finished at `date` ====="
exit 0;
#!/bin/bash

# Custom
. ./prep.sh || exit 1;

# Environment variables
. ./path.sh || exit 1;

. ./cmd.sh || exit 1;

# Config variables
stage=0
boost_silence=0.75
nj=4
lm_order=1 #unigram model
feat="mfcc"


echo "train_mode : $train_mode"

. utils/parse_options.sh || exit 1


./prep_noise_audio.sh

for x in train test 
do 
    utils/fix_data_dir.sh data/$x
        
done



# Setup feature directories
mfcc="feat/mfcc"

# Location of important files
data="data"
lang="$data/lang"
dict="$data/local/dict"

## Setup feature file directory
featdir=feat/$feat

#Scripts directories
steps="steps"
utils="utils"
# Experiment directory
exp="exp"

# print setup
echo "Features training are: ${feat}"

prep_lang=true

#lm_order=1 #unigram model

if [ "$prep_lang" = true ] ; then
    echo
    echo "===== PREPARING LANGUAGE DATA ====="
    echo
    # Needs to be prepared by hand (or using self written scripts):
    #
    # lexicon.txt [<word> <phone 1> <phone 2> ...]
    # nonsilence_phones.txt [<phone>]
    # silence_phones.txt [<phone>]
    # optional_silence.txt [<phone>]
    # Preparing language data
    utils/prepare_lang.sh data/local/dict "<UNK>" data/local/lang data/lang || exit 1;
    utils/validate_lang.pl data/lang/
    #exit 1;

    echo
    echo "===== LANGUAGE MODEL CREATION ====="
    echo "===== MAKING lm.arpa ====="
    echo

    loc=`which ngram-count`;
    if [ -z $loc ]; then
	    if uname -a | grep 64 >/dev/null; then
		    sdir=$KALDI_ROOT/tools/srilm/bin/i686-m64
	    else
		    sdir=$KALDI_ROOT/tools/srilm/bin/i686
	    fi
	    if [ -f $sdir/ngram-count ]; then
		    echo "Using SRILM language modelling tool from $sdir"
		    export PATH=$PATH:$sdir
	    else
		    echo "SRILM toolkit is probably not installed. Instructions: tools/install_srilm.sh"
		    exit 1
	    fi
    fi
    local=data/local
    mkdir $local/tmp
    ngram-count -order $lm_order -write-vocab $local/tmp/vocab-full.txt -wbdiscount -text $local/corpus.txt -lm $local/tmp/lm.arpa || exit 1

    echo
    echo "===== MAKING G.fst ====="
    echo

    lang=data/lang
    arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $local/tmp/lm.arpa > $lang/G.fst || exit 1;


fi

# Deleting previoulsy creating feat direcotry
echo Deleting previously creating feat directory!!
rm -rf $featdir
mkdir -p $featdir

fe_list="train test test_audio_0 test_audio_5 test_audio_10 test_audio_15 test_audio_20 test_audio_-5"

# Making mfcc and compute cmvn for train/test
for x in $fe_list; do

    mkdir -p $mfcc
    data2=$data/mfcc
    rm -rf $data2/$x/*
    mkdir -p $data2/$x
    cp -R $data/$x/* $data2/$x
    echo Hello "$data2/$x"
    echo "Making mfcc"
    $steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $data2/$x $exp/make_mfcc/$x $mfcc || exit 1
    # Compute CMVN stats
    echo "Making cmvn"
    $steps/compute_cmvn_stats.sh $data2/$x $exp/make_mfcc/$x $mfcc || exit 1

done


data=$data/$feat # ==> data/mfcc

echo data = $data

exp=$exp/$feat  # ==> exp/mfcc
rm -rf $exp
mkdir -p $exp


echo "stage 2 finished sleep for 3"
sleep 3


# GMM training
if [ $stage -le 3 ]; then
  echo ""
  echo "Stage ${stage}: Starting GMM training"
  #rm -rf $exp/* $data/train/split*
  rm $data/train/split*

  $steps/train_mono.sh --nj $nj --cmd "$train_cmd" \
    --boost_silence $boost_silence \
    $data/train $lang $exp/mono0a || exit 1;

  $utils/mkgraph.sh $lang $exp/mono0a $exp/mono0a/graph || exit 1;

  $steps/align_si.sh --nj $nj --cmd "$train_cmd" \
    --boost_silence $boost_silence \
    $data/train $lang $exp/mono0a $exp/mono0a_ali || exit 1;

  $steps/train_deltas.sh --cmd "$train_cmd" \
    --boost_silence $boost_silence \
    600 2500 $data/train $lang $exp/mono0a_ali $exp/tri1 || exit 1;

  $utils/mkgraph.sh $lang $exp/tri1 $exp/tri1/graph || exit 1;

  $steps/align_si.sh --nj $nj --cmd "$train_cmd" \
    $data/train $lang $exp/tri1 $exp/tri1_ali || exit 1;
  
  $steps/train_deltas.sh --cmd "$train_cmd" \
  --boost_silence $boost_silence \
    800 3500 $data/train $lang $exp/tri1_ali $exp/tri2a || exit 1;

  $utils/mkgraph.sh $lang $exp/tri2a $exp/tri2a/graph || exit 1;  

  $steps/align_si.sh --nj $nj --cmd "$train_cmd" \
  --use-graphs true  $data/train $lang $exp/tri2a $exp/tri2a_ali || exit 1;

  # LDA-MLLT
  $steps/train_lda_mllt.sh --cmd "$train_cmd" \
    --splice-opts "--left-context=5 --right-context=5" \
    1000 6500 $data/train $lang $exp/tri2a_ali $exp/tri2b || exit 1;

  $utils/mkgraph.sh $lang $exp/tri2b $exp/tri2b/graph || exit 1;

  $steps/align_si.sh --cmd "$train_cmd" --nj $nj \
    --use-graphs true $data/train $lang $exp/tri2b $exp/tri2b_ali || exit 1;
  # SAT TRAINING
  $steps/train_sat.sh \
    1500 6500 $data/train $lang $exp/tri2b_ali $exp/tri3b || exit 1;

  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    $data/train $lang $exp/tri3b $exp/tri3b_ali

  


  $utils/mkgraph.sh $lang $exp/tri3b $exp/tri3b/graph || exit 1;
fi


echo "stage 3 finished sleep for 3"
sleep 3


# GMM decoding
if [ $stage -le 4 ]; then

  model_list="tri3b"
  #model_list="tri3b"
  decode_list="test test_audio_0 test_audio_5 test_audio_10 test_audio_15 test_audio_20 test_audio_-5"
  echo ""
  echo "Stage ${stage}: Starting GMM decoding"
  for mdl in $model_list; do

    #for x in test; do
    for x in $decode_list; do
      echo ""
      echo "Evaluating model $mdl for $x"

      if [ ! -f $exp/$mdl/graph/HCLG.fst ]; then
        $utils/mkgraph.sh $lang $exp/$mdl $exp/$mdl/graph || exit 1;
      fi

      $steps/decode_fmllr.sh --cmd "$decode_cmd" --nj $nj --num-threads 4  \
          $exp/$mdl/graph $data/$x $exp/$mdl/decode_$x  #&

      wait

      echo ""
      echo "$x set decoded!"
      local/score.sh $data/$x $exp/$mdl/graph $exp/$mdl/decode_$x
    done

  done #mdl
fi #stage

echo "============================" >> results/${feat}_results
echo "Lm order:" $lm_order >> results/${feat}_results
echo "============================"
for x in exp/*/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh >> results/${feat}_results; done
for x in exp/*/*/decode*; do [ -d $x ] && grep SER $x/wer_* | utils/best_wer.sh >> results/${feat}_results; done

tail results/${feat}_results;

echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

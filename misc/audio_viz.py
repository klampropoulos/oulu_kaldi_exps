import numpy as np 
import librosa 
import matplotlib.pyplot as plt 
import seaborn as sns
import os
import scipy 
import librosa.display 
import sys
from math import log10


sns.set()


def read_wav(filename):
    sample_rate,samples = scipy.io.wavfile.read(filename)#[0]
    print(sample_rate)
    samples = samples[:, 0]
    return samples


def plot_wav(filename):
    "filename:str"
    sample_rate,samples = scipy.io.wavfile.read(filename)#[0]
    print(sample_rate)
    samples = samples[:, 0]
    print(type(samples))
    print(max(samples))
    print(samples.shape)
    samples_flt = samples.astype('float')
    print(samples_flt)
    #plt.figure(figsize=(12, 10))
    #librosa.display.waveplot(samples.astype('float'), sr=sample_rate)
    #plt.show()


def calc_ampl(array):
    mx_val=max(array)
    amplitude = mx_val / 32767
    print(amplitude)
    return amplitude
    
def rms(array):
    return np.sqrt(np.mean(np.abs(array) ** 2))

"""
def calc_snr(rmsampl1, rmsampl2):
    snr = (rmsampl1 / rmsampl2) ** 2
    print(f"snr is :{snr}")
"""


def calc_snr(power1_db, power2_db):
    #snrdb = power1_db - power2_db
    #print(f"snr is :{snrdb}")
    #snr_db = 10*log10(snr)
    #print(f"snr_dB :{snr_db}")
    pass


def power(array):
    power = np.sum(array**2) / len(array)
    power_db = 10 * log10(power)
    return power_db

def calc_dB(ampl):
    dB = 20 * log10(ampl)
    print(dB)
    return dB


def signaltonoise(a, axis, ddof): 
    a = np.asanyarray(a) 
    m = a.mean(axis) 
    sd = a.std(axis = axis, ddof = ddof) 
    return np.where(sd == 0, 0, m / sd) 


if __name__ == "__main__":
    print(sys.argv[1])
    #print(sys.argv[2])
    #plot_wav(sys.argv[1])
    #decibel = calc_dB(calc_ampl(read_wav(sys.argv[1])))
    #print(decibel)
    #rms1 = calc_ampl(read_wav(sys.argv[1]))
    #rms2 = calc_ampl(read_wav(sys.argv[2]))
    res = signaltonoise(read_wav(sys.argv[1]), axis=0, ddof=0)
    #calc_snr(rms1, rms2)
    print(res)
    #power1 = power(read_wav(sys.argv[1]))
    #power2 = power(read_wav(sys.argv[2]))
    #calc_snr(power1, power2)



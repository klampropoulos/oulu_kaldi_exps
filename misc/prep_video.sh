#! /bin/bash
. ./path.sh

. ./cmd.sh

nj=4
video="feat/video"
data=data
fe_list="train test"
view_list="1 2 3 4 5"
exp=exp
feat="video"	

featdir=feat/$feat


#rm -rf $featdir/*

#mkdir -p $featdir




for x in $fe_list; do

    for view in $view_list; do
        rm -rf $featdir_${view}
        mkdir -p $video_${view}
        data2=$data/video_${view}
        rm -rf $data2/$x/*
        mkdir -p $data2/$x
        cp -R $data/$x/* $data2/$x

        echo "Running make_video.sh"
        ./make_video.sh --nj $nj \
            --cmd "$train_cmd" \
            --audioRoot audio \
            --videoRoot ${x}_video_${view} \
            $data2/$x \
            $exp/make_video_${view}/$x \
            ${video}_${view} || exit 1

        steps/compute_cmvn_stats.sh $data2/$x $exp/make_video_${view}/$x/log $data2/$x/data || exit 1;
    done
done
#!/bin/bash


if [ ! -d "local" ] ; then

	prep.sh || exit 1;

fi

if [ ! -d "local/multi_condition" ] ; then

	cd local
	ln -s ../../aspire/s5/local/multi_condition . || exit 1;
	cd ..

fi

#cat <<EOF >multi_condition/.gitignore
# Ignore everything in this folder
#*
# except this file
#!.gitignore
#EOF


# check if the required tools are present
local/multi_condition/check_version.sh || exit 1;

if [ ! -d "RIRS_NOISES" ] ; then
  # Download the package that includes the real RIRs, simulated RIRs, isotropic noises and point-source noises
  #wget --no-check-certificate http://www.openslr.org/resources/28/rirs_noises.zip
  #unzip rirs_noises.zip
  ln -s ../rirs_noises/RIRS_NOISES . || exit 1;

fi


# Default rate
snrs="20:10"
foreground_snrs="20:10"
background_snrs="20:10"
num_data_reps=1
base_rirs="simurlated"
#base_rirs="simurlated"



# Default value
: "${data_dir:=test}"


rvb_opts=()

if [ "$base_rirs" == "simulated" ]; then
  # This is the config for the system using simulated RIRs and point-source noises
  #rvb_opts+=(--rir-set-parameters "0.5, RIRS_NOISES/simulated_rirs/smallroom/rir_list")
  #rvb_opts+=(--rir-set-parameters "0.5, RIRS_NOISES/simulated_rirs/mediumroom/rir_list")
  rvb_opts+=(--noise-set-parameters RIRS_NOISES/pointsource_noises/noise_list)
else
  # This is the config for the JHU ASpIRE submission system
  rvb_opts+=(--rir-set-parameters "1.0, RIRS_NOISES/real_rirs_isotropic_noises/rir_list")
  rvb_opts+=(--noise-set-parameters RIRS_NOISES/real_rirs_isotropic_noises/noise_list)
fi

# corrupt the oulu data to generate multi-condition data
# for data_dir in train dev test; do
for dir in data_dir; do
  if [ "$data_dir" == "train" ]; then
    num_reps=$num_data_reps
  else
    num_reps=1
  fi

  echo num_reps = "$num_reps"
  echo
  samp_rate=48000
  python3 steps/data/reverberate_data_dir.py \
    "${rvb_opts[@]}" \
    --prefix "rev" \
    --foreground-snrs $foreground_snrs \
    --background-snrs $background_snrs \
    --speech-rvb-probability 1 \
    --pointsource-noise-addition-probability 1 \
    --isotropic-noise-addition-probability 1 \
    --num-replications $num_reps \
    --max-noises-per-minute 1 \
    --source-sampling-rate $samp_rate \
    data/${data_dir} data/${data_dir}_rvb
done


echo "Data reverbation finished!!!!!"


echo
echo "===== $0 script is finished at `date` ====="
echo
exit 0;

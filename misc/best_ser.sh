!# /bin/bash

# This is a modified version of kaldi's ...wsj/utils/best_wer.sh file 
# Copyright 2010-2011 Microsoft Corporation
# Licensed under the Apache License, Version 2.0 (the "License");

py3_srcipt="
import sys
with open(sys.argv[1], 'r') as f:
    lines=f.readlines()
    a=[float(x.split(' ')[1]) for x in lines]
    minv=min(a)  # FOR ALL SERS ,NOT ONLY FIRST OCCURENCE
    ind=[i for i,v in enumerate(a) if v==minv ] 
    #ind=a.index(min(a))
    [print(lines[i]) for i in ind] 
"

tmp_f=tmp.txt
results_f=RESULTS
for x in exp/*/decode*; do
        
    [ -d $x ] && grep SER $x/wer_* |
    awk 'BEGIN{ FS="%SER"; } { if(NF == 2) { print FS$2" "$1; } else { print $0; }}' | \
    awk 'BEGIN{ FS="Sum/Avg"; } { if(NF == 2) { print $2" "$1; } else { print $0; }}' | \
    awk '{ if($1!~/%SER/) { print "%SER "$9" "$0; } else { print $0; }}' | \
    sed -e 's|\s\s*| |g' -e 's|\:$||' -e 's|\:\s*\|\s*$||' >> $x/$tmp_f  
    [ -f $x/$tmp_f  ] && python3 -c "$py3_srcipt" $x/$tmp_f #>> $results_f
    [ -f $x/$tmp_f  ] && rm $x/$tmp_f
done